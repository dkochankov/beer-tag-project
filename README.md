# BeerTag

##  Features
### Not-registered users:
* sees basic information of all beers: name, abv, style, country, average rating, link to details
* sees additional information about a beer: brewery and tags
* can login form where to authenticate
### Registered users:
* sees a list of beers they have drunk and want to drink
* can filter beers by country, style and tag
* can sort beers by abv, alphabetical and average rating
* can create a beer
* can add a tag to a beer
* can add a beer to either has drunk or want to drink list
* can logout
### Admin users:
* can see a list of all users
* can create, read, update and delete a user
* can update and delete a beer
##  Endpoints
### BeerMVC
* /beers: show a list of all beers, sort / filter with query params
* /beers/getBeerById/{beerid}: show the details of a beer
* /createBeer: create a beer
* /editBeer/{beerid}: edit a beer
* /deleteBeer/{beerid}: delete a beer
### UserMVC
* /users: show a list of users
* /getUserById/{userid}: show the details of a user
* /createUser: create a new user
* /editUser/{userid}: edit a user
* /deleteUser: delete a user
* /addToDrankList/{beerid}: adds a beer to the has drunk list of the user
* /addToWantToDrinkList/{beerid}: adds a beer to the want to drink list of the user
* /addRating/{beerid}: adds a rating to a beer
* /addTag/{beerid}: adds a tag to a beer
### HomeMVC
* /: redirect to beers page
##  Testing
Tests are testing the Service level. Line coverage is 87%.
##  Database
![Alt text](documentation/db-diagram.png?raw=true "Database diagram")
## Trello
https://trello.com/b/YcI5LV9v/beer-tag-team
![Alt text](documentation/beertag-trello.png)
## Authors
Kristina Kostova & Dimitar Kochankov