package com.telerikacademy.beertagproject.services;

import com.telerikacademy.beertagproject.models.Style;
import com.telerikacademy.beertagproject.repositories.interfaces.StyleRepository;
import com.telerikacademy.beertagproject.services.interfaces.StyleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class StyleServiceImpl implements StyleService {
    StyleRepository repository;

    @Autowired
    public StyleServiceImpl(StyleRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<Style> getAll() {
        return repository.getAll();
    }

    @Override
    public void create(Style style) {
        List<Style> styles = repository.getAll().stream()
                .filter(b -> b.getStyle().equals(style.getStyle()))
                .collect(Collectors.toList());
        if(styles.size() > 0) {
            throw new IllegalArgumentException(
                    String.format("Style with name %s already exists.", style.getStyle()));
        }
        repository.create(style);
    }

    @Override
    public Style update(int id, Style style) {
        return repository.update(id, style);
    }

    @Override
    public void delete(int id) {
        repository.delete(id);
    }
}
