package com.telerikacademy.beertagproject.services;

import com.telerikacademy.beertagproject.models.Brewery;
import com.telerikacademy.beertagproject.repositories.interfaces.BreweryRepository;
import com.telerikacademy.beertagproject.services.interfaces.BreweryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class BreweryServiceImpl implements BreweryService {
    private BreweryRepository breweryRepository;

    @Autowired
    public BreweryServiceImpl(BreweryRepository breweryRepository) {
        this.breweryRepository = breweryRepository;
    }

    @Override
    public List<Brewery> getAll() {
        return breweryRepository.getAll();
    }

    @Override
    public void create(Brewery brewery) {
        List<Brewery> countries = breweryRepository.getAll().stream()
                .filter(b -> b.getBrewery().equals(brewery.getBrewery()))
                .collect(Collectors.toList());
        if(countries.size() > 0) {
            throw new IllegalArgumentException(
                    String.format("Brewery with name %s already exists.", brewery.getBrewery()));
        }
        breweryRepository.create(brewery);
    }

    @Override
    public Brewery update(int id, Brewery brewery) {
        return breweryRepository.update(id, brewery);
    }

    @Override
    public void delete(int id) {
        breweryRepository.delete(id);
    }
}
