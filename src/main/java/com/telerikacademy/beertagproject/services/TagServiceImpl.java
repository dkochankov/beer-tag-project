package com.telerikacademy.beertagproject.services;

import com.telerikacademy.beertagproject.models.Tag;
import com.telerikacademy.beertagproject.repositories.interfaces.TagRepository;
import com.telerikacademy.beertagproject.services.interfaces.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class TagServiceImpl implements TagService {
    private TagRepository tagRepository;

    @Autowired
    public TagServiceImpl(TagRepository repository) {
        this.tagRepository = repository;
    }

    @Override
    public List<Tag> getAll() {
        return tagRepository.getAll();
    }

    @Override
    public Tag getTagById(int id) {
        try {
            Tag tag = tagRepository.getTagById(id);

            if (tag == null) {
                throw new IllegalArgumentException(String.format("Tag with id %d not found.", id));
            }
            return tag;
        } catch (Exception e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public List<Tag> getTagsByBeerId(int id)
    {
        return tagRepository.getTagsByBeerId(id);
    }

    @Override
    public void create(Tag tag) {
        List<Tag> tags = tagRepository.getAll().stream()
                .filter(b -> b.getTag().equals(tag.getTag()))
                .collect(Collectors.toList());
        if(tags.size() > 0) {
            throw new IllegalArgumentException(
                    String.format("Tag with name %s already exists.", tag.getTag()));
        }
        tagRepository.create(tag);
    }

    @Override
    public Tag update(int id, Tag tag) {
        return tagRepository.update(id, tag);
    }

    @Override
    public void delete(int id) {
        tagRepository.delete(id);
    }
}
