package com.telerikacademy.beertagproject.services;

import com.telerikacademy.beertagproject.models.Country;
import com.telerikacademy.beertagproject.repositories.interfaces.CountryRepository;
import com.telerikacademy.beertagproject.services.interfaces.CountryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CountryServiceImpl implements CountryService {
    private CountryRepository repository;

    @Autowired
    public CountryServiceImpl(CountryRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<Country> getAll() {
        return repository.getAll();
    }

    @Override
    public void create(Country country) {
        List<Country> countries = repository.getAll().stream()
                .filter(b -> b.getCountry().equals(country.getCountry()))
                .collect(Collectors.toList());
        if(countries.size() > 0) {
            throw new IllegalArgumentException(
                    String.format("Country with name %s already exists.", country.getCountry()));
        }
        repository.create(country);
    }

    @Override
    public Country update(int id, Country country) {
        return repository.update(id, country);
    }

    @Override
    public void delete(int id) {
        repository.delete(id);
    }
}
