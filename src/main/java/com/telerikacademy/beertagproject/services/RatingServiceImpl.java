package com.telerikacademy.beertagproject.services;

import com.telerikacademy.beertagproject.models.Rating;
import com.telerikacademy.beertagproject.models.Tag;
import com.telerikacademy.beertagproject.models.User;
import com.telerikacademy.beertagproject.repositories.interfaces.RatingRepository;
import com.telerikacademy.beertagproject.services.interfaces.RatingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class RatingServiceImpl implements RatingService {
    RatingRepository ratingRepository;

    @Autowired
    public RatingServiceImpl(RatingRepository repository) {
        this.ratingRepository = repository;
    }

    @Override
    public List<Rating> getAll() {
        return ratingRepository.getAll();
    }

    @Override
    public Rating getRatingById(int id) {
        try {
            Rating rating = ratingRepository.getRatingById(id);

            if (rating == null) {
                throw new IllegalArgumentException(String.format("Rating with id %d not found.", id));
            }
            return rating;
        } catch (Exception e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public Rating getRatingByUserIdAndBeerId(int userid, int beerid){
        return ratingRepository.getRatingByUserIdandBeerId(userid, beerid);
    }

    @Override
    public void create(Rating rating) {
        List<Rating> countries = ratingRepository.getAll().stream()
                .filter(b -> b.getRating() == rating.getRating())
                .collect(Collectors.toList());
        if(countries.size() > 0) {
            throw new IllegalArgumentException(
                    String.format("Rating with name %s already exists.", rating.getRating()));
        }
        ratingRepository.create(rating);
    }

    @Override
    public Rating update(int id, Rating rating) {
        return ratingRepository.update(id, rating);
    }

    @Override
    public void delete(int id) {
        ratingRepository.delete(id);
    }
}
