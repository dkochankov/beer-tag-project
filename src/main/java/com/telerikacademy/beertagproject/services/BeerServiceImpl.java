package com.telerikacademy.beertagproject.services;

import com.telerikacademy.beertagproject.models.*;
import com.telerikacademy.beertagproject.repositories.StyleRepositoryImpl;
import com.telerikacademy.beertagproject.repositories.interfaces.*;
import com.telerikacademy.beertagproject.services.interfaces.BeerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import javax.transaction.Transactional;
import java.security.Principal;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class BeerServiceImpl implements BeerService {
    BeerRepository beerRepository;
    CountryRepository countryRepository;
    StyleRepository styleRepository;
    TagRepository tagRepository;
    RatingRepository ratingRepository;

    @Autowired
    public BeerServiceImpl(BeerRepository beerRepository, CountryRepository countryRepository,
                           StyleRepository styleRepository, TagRepository tagRepository,
                           RatingRepository ratingRepository) {
        this.beerRepository = beerRepository;
        this.countryRepository = countryRepository;
        this.styleRepository = styleRepository;
        this.tagRepository = tagRepository;
        this.ratingRepository = ratingRepository;
    }

    @Override
    public List<Beer> getAll() {
        return beerRepository.getAll();
    }

    @Override
    public Beer getBeerById(int id) {
        try {
            Beer beer = beerRepository.getBeerById(id);

            if (beer == null) {
                throw new IllegalArgumentException(String.format("Beer with id %d not found.", id));
            }
            return beer;
        } catch (Exception e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public List<Beer> getAll(String sortBy) {
        if (sortBy.equals("abv")) {
            return beerRepository.getAllSortByABV();
        } else if (sortBy.equals("alphabetical")) {
            return beerRepository.getAllSortByAlphabetical();
        } else if (sortBy.equals("rating")) {
            return beerRepository.getAllSortByAverageRating();
        } else {
            return beerRepository.getAll();
        }
    }

    @Override
    public List<Beer> getAllFilteredByCountry(String countryName) {
        try {
            Country country = countryRepository.getCountryByName(countryName);
            List<Beer> countryBeers = beerRepository.getAllFilteredByCountry(country.getId());
            return countryBeers;
        } catch (Exception e) {
            throw new IllegalArgumentException(
                    String.format("A country with name %s doesn't exist.", countryName)
            );
        }
    }

    @Override
    public List<Beer> getAllFilteredByStyle(String styleName) {
        try {
            Style style = styleRepository.getStyleByName(styleName);
            return beerRepository.getAllFilteredByStyle(style.getId());
        } catch (Exception e) {
            throw new IllegalArgumentException(
                    String.format("A style with name %s doesn't exist.", styleName)
            );
        }
    }

    @Override
    public List<Beer> getAllFilteredByTag(String tagName) {
        try {
            Tag tag = tagRepository.getTagByName(tagName);
            return beerRepository.getAllFilteredByTag(tag.getId());
        } catch (Exception e) {
            throw new IllegalArgumentException(
                    String.format("A tag with name %s doesn't exist.", tagName)
            );
        }
    }

    @Override
    public void create(Beer beer) {
        List<Beer> beers = beerRepository.getAll().stream()
                .filter(b -> b.getName().equals(beer.getName()))
                .collect(Collectors.toList());
        if(beers.size() > 0) {
            throw new IllegalArgumentException(
                    String.format("Beer with name %s already exists.", beer.getName()));
        } try {
            beerRepository.create(beer); }
        catch (Exception e) {
            throw new IllegalArgumentException("Missing or invalid data in one or more fields.");
        }
    }

    @Override
    public Beer update(int id, Beer beer) {
        return beerRepository.update(id, beer);
    }

    @Override
    public void delete(int id) {
        beerRepository.delete(id);
    }

    @Override
    public void deleteWishesBeers(int userid, int beerid) {
        beerRepository.deleteWishesBeers(userid, beerid);
    }


}
