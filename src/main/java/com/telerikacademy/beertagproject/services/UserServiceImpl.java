package com.telerikacademy.beertagproject.services;

import com.telerikacademy.beertagproject.models.Beer;
import com.telerikacademy.beertagproject.models.Rating;
import com.telerikacademy.beertagproject.models.Tag;
import com.telerikacademy.beertagproject.models.User;
import com.telerikacademy.beertagproject.repositories.BeerRepositoryImpl;
import com.telerikacademy.beertagproject.repositories.RatingRepositoryImpl;
import com.telerikacademy.beertagproject.repositories.UserRepositoryImpl;
import com.telerikacademy.beertagproject.repositories.interfaces.BeerRepository;
import com.telerikacademy.beertagproject.repositories.interfaces.RatingRepository;
import com.telerikacademy.beertagproject.repositories.interfaces.TagRepository;
import com.telerikacademy.beertagproject.repositories.interfaces.UserRepository;
import com.telerikacademy.beertagproject.services.interfaces.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {
    private UserRepository userRepository;
    private RatingRepository ratingRepository;
    private BeerRepository beerRepository;
    private TagRepository tagRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, RatingRepository ratingRepository,
                           BeerRepository beerRepository, TagRepository tagRepository) {
        this.userRepository = userRepository;
        this.ratingRepository = ratingRepository;
        this.beerRepository = beerRepository;
        this.tagRepository = tagRepository;
    }

    @Override
    public List<User> listUsers() {
        return userRepository.listUsers();
    }

    @Override
    public User getUserByName(String name) {
        User user = userRepository.getUserByName(name);
        if (user == null) {
            throw new IllegalArgumentException(String.format("user with name %s not found.", name));
        }
        return user;
    }

    @Override
    public User getUserById(int id) {
        User user = userRepository.getUserById(id);
        if (user == null) {
            throw new IllegalArgumentException(String.format("user with id %d not found.", id));
        }
        return user;
    }

    @Override
    public void createUser(User user, String authority) {
        if (user == null) {
            throw new IllegalArgumentException("Missing data in one or more fields!");
        }
        if(user.getCountry() == null) {
            throw new IllegalArgumentException("Missing data in one or more fields!");
        }
        if (user.getUsername().isEmpty() || user.getMail().isEmpty() || user.getPassword().isEmpty()) {
            throw new IllegalArgumentException("Missing data in one or more fields!");
        }
        List<User> users = userRepository.listUsers().stream()
                .filter(x -> x.getUsername().equals(user.getUsername()))
                .collect(Collectors.toList());

        if (users.size() > 0) {
            throw new IllegalArgumentException(
                    String.format("User with name %s already exists", user.getUsername()));
        }
        userRepository.createUser(user, authority);
    }

    @Override
    public void updateUser(int id, User user, String authority) {
        if (user == null) {
            throw new IllegalArgumentException("Missing data in one or more fields!");
        }
        if(user.getCountry() == null) {
            throw new IllegalArgumentException("Missing data in one or more fields!");
        }
        if(user.getMail().isEmpty() || user.getPassword().isEmpty()) {
            throw new IllegalArgumentException("Missing data in one or more fields!");
        }
        userRepository.updateUser(id, user, authority);
    }

    @Override
    public void deleteUser(int id) {
        userRepository.deleteUser(id);
    }

    @Override
    public void addToDrankList(int userid, int beerid) {
        List<Beer> beers = getDrankList(userid).stream()
                .filter(x -> x.getId() == (beerid))
                .collect(Collectors.toList());
        if (beers.size() > 0) {
            throw new IllegalArgumentException(
                    String.format("Beer with id %d already added", beerid));
        }
        userRepository.addToDrankList(userid, beerid);
    }

    @Override
    public void addToWantToDrinkList(int userid, int beerid) {
        List<Beer> beers = getWantToDrinkList(userid).stream()
                .filter(x -> x.getId() == (beerid))
                .collect(Collectors.toList());
        if (beers.size() > 0) {
            throw new IllegalArgumentException(
                    String.format("Beer with id %d already added", beerid));
        }
        userRepository.addToWantToDrinkList(userid, beerid);
    }

    @Override
    public void addRating(int rating, int userid, int beerid) {
        if (userRepository.getRating(userid, beerid).size() > 0) {
            userRepository.updateRating(rating, userid, beerid);
        } else {
            userRepository.addRating(rating, userid, beerid);
        }

        List<Rating> beerRatings = ratingRepository.getBeerRatings(beerid);
        if (!beerRatings.isEmpty()) {
            double averageRating = beerRatings
                    .stream()
                    .mapToDouble(r -> r.getRating())
                    .average()
                    .getAsDouble();
            beerRepository.updateAverageRating(beerid, averageRating);
        }
    }

    public void addTag(String tagName, int beerid) {
        try {
            Tag tag = tagRepository.getTagByName(tagName);
            if (tag == null) {
                Tag newTag = new Tag(tagName);
                tagRepository.create(newTag);
            }
            int tagid = tagRepository.getTagByName(tagName).getId();
            beerRepository.addTagToBeer(tagid, beerid);
        } catch (Exception e) {
            throw new IllegalArgumentException();
        }
    }

    @Override
    public List<Beer> getDrankList(int userid) { return userRepository.getDrankList(userid);}

    @Override
    public List<Beer> getWantToDrinkList(int userid) { return userRepository.getWantToDrinkList(userid);}

}