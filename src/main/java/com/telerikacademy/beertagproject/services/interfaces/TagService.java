package com.telerikacademy.beertagproject.services.interfaces;

import com.telerikacademy.beertagproject.models.Tag;

import java.util.List;

public interface TagService {
    List<Tag> getAll();
    Tag getTagById(int id);
    List<Tag> getTagsByBeerId(int id);
    void create(Tag tag);
    Tag update(int id, Tag tag);
    void delete(int id);
}
