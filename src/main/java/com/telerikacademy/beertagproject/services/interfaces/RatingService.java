package com.telerikacademy.beertagproject.services.interfaces;

import com.telerikacademy.beertagproject.models.Rating;

import java.util.List;

public interface RatingService {
    List<Rating> getAll();
    Rating getRatingById(int id);
    Rating getRatingByUserIdAndBeerId(int userid, int beerid);
    void create(Rating rating);
    Rating update(int id, Rating rating);
    void delete(int id);
}
