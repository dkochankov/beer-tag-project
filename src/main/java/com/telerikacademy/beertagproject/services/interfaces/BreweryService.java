package com.telerikacademy.beertagproject.services.interfaces;

import com.telerikacademy.beertagproject.models.Brewery;

import java.util.List;

public interface BreweryService {
    List<Brewery> getAll();
    void create(Brewery brewery);
    Brewery update(int id, Brewery brewery);
    void delete(int id);
}
