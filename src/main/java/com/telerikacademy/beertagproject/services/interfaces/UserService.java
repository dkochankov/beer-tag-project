package com.telerikacademy.beertagproject.services.interfaces;

import com.telerikacademy.beertagproject.models.Beer;
import com.telerikacademy.beertagproject.models.User;

import java.util.List;

public interface UserService {
    List<User> listUsers();
    User getUserByName(String name);
    User getUserById(int id);
    void createUser(User user, String authority);
    void updateUser(int id, User user, String authority);
    void deleteUser(int id);

    void addToDrankList(int userid, int beerid);
    void addToWantToDrinkList(int userid, int beerid);
    void addRating(int rating, int userid, int beerid);
    void addTag(String tag, int beerid);
    List<Beer> getDrankList(int userid);
    List<Beer> getWantToDrinkList(int userid);
}
