package com.telerikacademy.beertagproject.services.interfaces;

import com.telerikacademy.beertagproject.models.Beer;

import java.util.List;

public interface BeerService {
    List<Beer> getAll();
    Beer getBeerById(int id);
    List<Beer> getAll(String filterName);
    List<Beer> getAllFilteredByCountry(String country);
    List<Beer> getAllFilteredByStyle(String style);
    List<Beer> getAllFilteredByTag(String tag);
    void deleteWishesBeers(int userid, int beerid);
    void create(Beer beer);
    Beer update(int id, Beer beer);
    void delete(int id);
}
