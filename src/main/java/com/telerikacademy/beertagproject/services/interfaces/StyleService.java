package com.telerikacademy.beertagproject.services.interfaces;

import com.telerikacademy.beertagproject.models.Style;

import java.util.List;

public interface StyleService {
    List<Style> getAll();
    void create(Style style);
    Style update(int id, Style style);
    void delete(int id);
}
