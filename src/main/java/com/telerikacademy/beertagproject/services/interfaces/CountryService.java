package com.telerikacademy.beertagproject.services.interfaces;

import com.telerikacademy.beertagproject.models.Country;

import java.util.List;

public interface CountryService {
    List<Country> getAll();
    void create(Country country);
    Country update(int id, Country country);
    void delete(int id);
}
