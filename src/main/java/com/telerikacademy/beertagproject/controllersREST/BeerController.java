package com.telerikacademy.beertagproject.controllersREST;

import com.telerikacademy.beertagproject.models.Beer;
import com.telerikacademy.beertagproject.services.interfaces.BeerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/beers")
public class BeerController {
    private BeerService service;

    @Autowired
    public BeerController(BeerService service) {
        this.service = service;
    }

    @GetMapping
    //@RequestParam(required = false) -> V zaqvkata ne e zadaljitelno da ima v URLa parametyr.
    public List<Beer> getAll(@RequestParam(required = false) String sortBy,
                             @RequestParam(required = false) String country,
                             @RequestParam(required = false) String style,
                             @RequestParam(required = false) String tag) {

        if (country != null) {
            return service.getAllFilteredByCountry(country);
        }
        if (style != null) {
            return service.getAllFilteredByStyle(style);
        }
        if (tag != null) {
            return service.getAllFilteredByTag(tag);
        }
        if (sortBy == null) {
            return service.getAll();
        }
        return service.getAll(sortBy);
    }

    @PostMapping("/new")
    public Beer create(@Valid @RequestBody Beer beer) {
        try {
            service.create(beer);
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
        return beer;
    }

    @PutMapping("/{id}")
    public Beer update(@Valid @PathVariable int id, @Valid @RequestBody Beer beer) {
        try {
            return service.update(id, beer);
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@Valid @PathVariable int id) {
        service.delete(id);
    }


}
