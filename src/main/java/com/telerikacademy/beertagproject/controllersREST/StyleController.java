package com.telerikacademy.beertagproject.controllersREST;

import com.telerikacademy.beertagproject.models.Style;
import com.telerikacademy.beertagproject.services.interfaces.StyleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/styles")
public class StyleController {
    private StyleService service;

    @Autowired
    public StyleController(StyleService service) {
        this.service = service;
    }

    @GetMapping
    public List<Style> getAll() {
        return service.getAll();
    }

    @PostMapping("/new")
    public Style create(@Valid @RequestBody Style style) {
        try {
            service.create(style);
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
        return style;
    }

    @PutMapping("/{id}")
    public Style update(@Valid @PathVariable int id, @Valid @RequestBody Style style) {
        try {
            return service.update(id, style);
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@Valid @PathVariable int id) {
        service.delete(id);
    }


}
