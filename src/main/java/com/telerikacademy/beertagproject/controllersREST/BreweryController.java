package com.telerikacademy.beertagproject.controllersREST;

import com.telerikacademy.beertagproject.models.Brewery;
import com.telerikacademy.beertagproject.services.interfaces.BreweryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/breweries")
public class BreweryController {
    private BreweryService service;

    @Autowired
    public BreweryController(BreweryService service) {
        this.service = service;
    }

    @GetMapping
    public List<Brewery> getAll() {
        return service.getAll();
    }

    @PostMapping("/new")
    public Brewery create(@Valid @RequestBody Brewery brewery) {
        try {
            service.create(brewery);
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
        return brewery;
    }

    @PutMapping("/{id}")
    public Brewery update(@Valid @PathVariable int id, @Valid @RequestBody Brewery brewery) {
        try {
            return service.update(id, brewery);
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@Valid @PathVariable int id) {
        service.delete(id);
    }
}
