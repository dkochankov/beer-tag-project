package com.telerikacademy.beertagproject.controllersREST;

import com.telerikacademy.beertagproject.models.Rating;
import com.telerikacademy.beertagproject.services.interfaces.RatingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/ratings")
public class RatingController {
    private RatingService service;


    @Autowired
    public RatingController(RatingService service) {
        this.service = service;
    }

    @GetMapping
    public List<Rating> getAll() {
        return service.getAll();
    }

    @GetMapping("/{id}")
    public Rating getRatingById(@PathVariable int id) {
        try {
            return service.getRatingById(id);
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping("/new")
    public Rating create(@Valid @RequestBody Rating rating) {
        try {
            service.create(rating);
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
        return rating;
    }

    @PutMapping("/{id}")
    public Rating update(@Valid @PathVariable int id, @Valid @RequestBody Rating rating) {
        try {
            return service.update(id, rating);
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@Valid @PathVariable int id) {
        service.delete(id);
    }

}