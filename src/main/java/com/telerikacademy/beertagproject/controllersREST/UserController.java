package com.telerikacademy.beertagproject.controllersREST;

import com.telerikacademy.beertagproject.models.User;
import com.telerikacademy.beertagproject.services.BeerServiceImpl;
import com.telerikacademy.beertagproject.services.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/users")
public class UserController {
    private UserServiceImpl userService;
    private BeerServiceImpl beerService;


    @Autowired
    public UserController(UserServiceImpl userService) {
        this.userService = userService;
    }

    @GetMapping
    public List<User> getAll(@RequestParam(required = false) String name) {
        return userService.listUsers();
    }

    @GetMapping("/{id}")
    public User getUserById(@PathVariable int id) {
        try {
            return userService.getUserById(id);
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/{name}")
    public User getUserByName(@PathVariable String name) {
        try {
            return userService.getUserByName(name);
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping("/createUser")
    @ResponseStatus(HttpStatus.CREATED)
    public @ResponseBody User createUser(@Valid @RequestBody User user) {
        try {
            userService.createUser(user, "ROLE_USER");
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
        return user;
    }

    /*
    @PutMapping("/updateUser/{id}")
    public User updateUser(@PathVariable int id, @Valid @RequestBody User user) {
        return userService.updateUser(id, user);
    }
*/

    @DeleteMapping("/deleteUser/{id}")
    public void deleteUser(@PathVariable int id) {
        userService.deleteUser(id);
    }


    //////////// Business logic ///////////////////

    /*
    @GetMapping("markBeerAsDrank/{userName}{beerName}")
    public String markBeerAsDrank(@PathVariable String userName, @PathVariable String beerName) {
        try {
            //projService.getProjectById(id1).addEmployee(empService.getById(id2));
            //empService.getById(id2).addProject(projService.getProjectById(id1));
            userService.getByName(userName).

            ;beerService.getByName()
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
        return String.format("employee with id %s added to project wit id %s", id1, id2);

        //return userService.listUsers();
    }
*/

    /////////////////////// Business logic //////////////////////
/*
    @PostMapping("/addRating")
    @ResponseStatus(HttpStatus.CREATED)
    public void addRating(@Valid @RequestBody Rating rating, String beerName)
    {

    }

    @PostMapping("/createUser")
    @ResponseStatus(HttpStatus.CREATED)
    public void addTag(@Valid @RequestBody Tag tag, String beerName)
    {

    }
*/
}
