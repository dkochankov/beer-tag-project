package com.telerikacademy.beertagproject.controllersREST;

import com.telerikacademy.beertagproject.models.Country;
import com.telerikacademy.beertagproject.services.interfaces.CountryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/countries")
public class CountryController {
    private CountryService service;

    @Autowired
    public CountryController(CountryService service) {
        this.service = service;
    }

    @GetMapping
    public List<Country> getAll() {
        return service.getAll();
    }

    @PostMapping("/new")
    public Country create(@Valid @RequestBody Country country) {
        try {
            service.create(country);
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
        return country;
    }

    @PutMapping("/{id}")
    public Country update(@Valid @PathVariable int id, @Valid @RequestBody Country country) {
        try {
            return service.update(id, country);
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@Valid @PathVariable int id) {
        service.delete(id);
    }


}

