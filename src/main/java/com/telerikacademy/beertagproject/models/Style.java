package com.telerikacademy.beertagproject.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@Entity
@Table(name = "styles")
public class Style {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "styleid")
    private int id;

    @NotNull
    @Size(min = 3, max = 20, message = "Name must be between 3 and 20 characters")
    @Column(name = "style")
    private String style;

    @OneToMany
    @JoinColumn(name = "styleid")
    @JsonIgnore
    private List<Beer> beers;

    public Style() {

    }

    public Style(String style) {
        this.style = style;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStyle() {
        return style;
    }

    public void setStyle(String style) {
        this.style = style;
    }

    public List<Beer> getBeers() {
        return beers;
    }

    public void setBeers(List<Beer> beers) {
        this.beers = beers;
    }
}
