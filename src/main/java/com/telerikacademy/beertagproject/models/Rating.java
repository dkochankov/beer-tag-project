package com.telerikacademy.beertagproject.models;

import org.hibernate.validator.constraints.Range;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "ratings")
public class Rating {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ratingid")
    private int id;

    @NotNull
    @Range(min = 1, max = 5)
    @Column(name = "rating")
    private int rating;

    @ManyToOne
    @JoinColumn(name = "userid")
    private User author;

    @ManyToOne
    @JoinColumn(name = "beerid")
    private Beer beer;

    public Rating() {
    }

    public Rating(int rating, User author, Beer beer) {
        this.rating = rating;
        this.author = author;
        this.beer = beer;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public User getAuthor() {
        return author;
    }

    public void setAuthor(User author) {
        this.author = author;
    }

    public Beer getBeer() {
        return beer;
    }

    public void setBeer(Beer beer) {
        this.beer = beer;
    }
}
