package com.telerikacademy.beertagproject.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@Entity
@Table(name = "users")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "userid")
    private int id;

    @NotNull(message = "field required")
    @Size(min = 2, max = 20, message = "Name must be between 2 and 20 symbols.")
    @Column(name = "username")
    private String username;

    @NotNull(message = "field required")
    @Size(min = 2, max = 20, message = "Password must be between 2 and 20 symbols.")
    @Column(name = "password")
    private String password;

    @Column(name = "picture")
    private String picture;

    @NotNull(message = "field required")
    @Column(name = "mail")
    private String mail;

    @ManyToOne
    @NotNull(message = "field required")
    @JoinColumn(name = "countryid")
    private Country country;

    @OneToMany
    @JoinColumn(name = "userid")
    @JsonIgnore
    private List<Rating> ratings;

    @ManyToMany
    @JoinTable(
            name = "user_beers_want_to_drink_rel",
            joinColumns = @JoinColumn(name = "userid"),
            inverseJoinColumns = @JoinColumn(name = "beerid")
    )
    @JsonIgnore
    private List<Beer> wantToDrinkBeers;

    @ManyToMany //(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinTable(
            name = "user_beers_drank_rel",
            joinColumns = @JoinColumn(name = "userid"),
            inverseJoinColumns = @JoinColumn(name = "beerid")
    )
    @JsonIgnore
    private List<Beer> drankBeers;

    @Column(name = "enabled")
    private boolean enabled = true;

    public User() {

    }

    public User(String username, String password, String picture, String mail, Country country) {
        this.username = username;
        this.password = password;
        this.picture = picture;
        this.mail = mail;
        this.country = country;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public List<Rating> getRatings() {
        return ratings;
    }

    public void setRatings(List<Rating> ratings) {
        this.ratings = ratings;
    }

    public List<Beer> getWantToDrinkBeers() {
        return wantToDrinkBeers;
    }

    public void setWantToDrinkBeers(List<Beer> wantToDrinkBeers) {
        this.wantToDrinkBeers = wantToDrinkBeers;
    }

    public List<Beer> getDrankBeers() {
        return drankBeers;
    }

    public void setDrankBeers(List<Beer> drankBeers) {
        this.drankBeers = drankBeers;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public void addToDrankList(Beer beer)
    {
        this.getDrankBeers().add(beer);
    }

    public void addToWantToDrinkList(Beer beer) { this.getWantToDrinkBeers().add(beer); }

    public void addRating(Rating rating)
    {
        this.getRatings().add(rating);
    }
}
