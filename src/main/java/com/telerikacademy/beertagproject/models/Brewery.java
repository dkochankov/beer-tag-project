package com.telerikacademy.beertagproject.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@Entity
@Table(name = "breweries")
public class Brewery {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "breweryid")
    private int id;

    @NotNull
    @Size(min = 3, max = 20, message = "Name must be between 3 and 20 characters")
    @Column(name = "brewery")
    private String brewery;

    @OneToMany
    @JoinColumn(name = "breweryid")
    @JsonIgnore
    private List<Beer> beers;

    public Brewery() {

    }

    public Brewery(String brewery) {
        this.brewery = brewery;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBrewery() {
        return brewery;
    }

    public void setBrewery(String brewery) {
        this.brewery = brewery;
    }

    public List<Beer> getBeers() {
        return beers;
    }

    public void setBeers(List<Beer> beers) {
        this.beers = beers;
    }
}
