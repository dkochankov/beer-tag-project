package com.telerikacademy.beertagproject.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.validation.constraints.NotNull;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.List;

@Entity
@Table(name = "beers")
public class Beer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "beerid")
    private int id;

    @Column(name = "beername")
    @NotNull(message = "field required")
    @Size(min = 3, max = 20, message = "Name must be between 3 and 20 characters.")
    private String name;

    @Column(name = "abv")
    @NotNull(message = "field required")
    private double abv;

    @Column(name = "picture")
    private String picture;

    @Column(name = "description")
    @NotNull(message = "field required")
    private String description;

    @Column(name = "averagerating")
    private double averageRating;

    @ManyToOne
    @NotNull(message = "field required")
    @JoinColumn(name = "styleid")
    private Style style;

    @ManyToOne
    @NotNull(message = "field required")
    @JoinColumn(name = "countryid")
    private Country country;

    @ManyToOne
    @NotNull(message = "field required")
    @JoinColumn(name = "breweryid")
    private Brewery brewery;

    @OneToMany
    @JoinColumn(name = "beerid")
    @JsonIgnore
    private List<Rating> ratings;

    @ManyToMany(mappedBy = "beers")
    @JsonIgnore
    private List<Tag> tags;

    @ManyToMany(mappedBy = "wantToDrinkBeers")
    @JsonIgnore
    private List<User> wantToDrinkUsers;

    @ManyToMany(mappedBy = "drankBeers")
    @JsonIgnore
    private List<User> drankUsers;

    public Beer() {

    }

    public Beer(String name, double abv, String picture, String description,
                double averageRating, Style style, Country country, Brewery brewery) {
        this.name = name;
        this.abv = abv;
        this.picture = picture;
        this.description = description;
        this.averageRating = averageRating;
        this.style = style;
        this.country = country;
        this.brewery = brewery;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Brewery getBrewery() {
        return brewery;
    }

    public void setBrewery(Brewery brewery) {
        this.brewery = brewery;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public double getAbv() {
        return abv;
    }

    public void setAbv(double abv) {
        this.abv = abv;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getAverageRating() {
        return averageRating;
    }

    public void setAverageRating(double averageRating) {
        this.averageRating = averageRating;
    }

    public Style getStyle() {
        return style;
    }

    public void setStyle(Style style) {
        this.style = style;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public List<Rating> getRatings() {
        return ratings;
    }

    public void setRatings(List<Rating> ratings) {
        this.ratings = ratings;
    }

    public List<Tag> getTags() {
        return tags;
    }

    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }

    public List<User> getWantToDrinkUsers() {
        return wantToDrinkUsers;
    }

    public void setWantToDrinkUsers(List<User> wantToDrinkUsers) {
        this.wantToDrinkUsers = wantToDrinkUsers;
    }

    public List<User> getDrankUsers() {
        return drankUsers;
    }

    public void setDrankUsers(List<User> drankUsers) {
        this.drankUsers = drankUsers;
    }
}
