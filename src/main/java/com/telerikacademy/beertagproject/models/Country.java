package com.telerikacademy.beertagproject.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@Entity
@Table(name = "countries")
public class Country {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "countryid")
    private int id;

    @NotNull
    @Size(min = 3, max = 20, message = "Name must be between 3 and 20 characters")
    @Column(name = "country")
    private String country;

    @OneToMany
    @JoinColumn(name = "countryid")
    @JsonIgnore
    private List<User> users;

    @OneToMany
    @JoinColumn(name = "countryid")
    @JsonIgnore
    private List<Beer> beers;

    public Country() {

    }

    public Country(String country) {
        this.country = country;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    public List<Beer> getBeers() {
        return beers;
    }

    public void setBeers(List<Beer> beers) {
        this.beers = beers;
    }
}
