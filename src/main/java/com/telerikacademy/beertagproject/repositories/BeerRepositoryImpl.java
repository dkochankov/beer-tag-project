package com.telerikacademy.beertagproject.repositories;

import com.telerikacademy.beertagproject.models.Beer;
import com.telerikacademy.beertagproject.repositories.interfaces.BeerRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public class BeerRepositoryImpl implements BeerRepository {
    private SessionFactory sessionFactory;

    @Autowired
    public BeerRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Beer getBeerById(int id) {
        Session session = setupSession();
        return session.get(Beer.class, id);
    }

    @Override
    public List<Beer> getAll() {
        Session session = setupSession();
        Query<Beer> query = session.createQuery("from Beer", Beer.class);
        return query.list();
    }

    @Override
    public List<Beer> getAllSortByABV() {
        Session session = setupSession();
        Query<Beer> query = session.createQuery("from Beer order by abv", Beer.class);
        return query.list();
    }

    @Override
    public List<Beer> getAllSortByAlphabetical() {
        Session session = setupSession();
        Query<Beer> query = session.createQuery("from Beer order by name", Beer.class);
        return query.list();
    }

    @Override
    public List<Beer> getAllSortByAverageRating() {
        Session session = setupSession();
        Query<Beer> query = session.createQuery(
                "from Beer order by averagerating desc", Beer.class);
        return query.list();
    }

    public List<Beer> getAllFilteredByCountry(int countryId) {
        Session session = setupSession();
        Query<Beer> query = session.createQuery("from Beer where country.id = :countryId");
        query.setParameter("countryId", countryId);
        return query.list();
    }

    @Override
    public List<Beer> getAllFilteredByStyle(int styleId) {
        Session session = setupSession();
        Query<Beer> query = session.createQuery("from Beer where style.id = :styleId");
        query.setParameter("styleId", styleId);
        return query.list();
    }

    @Override
    public List<Beer> getAllFilteredByTag(int tagId) {
        Session session = setupSession();
        Query<Beer> query = session.createQuery("select b from Beer as b join b.tags as t where t.id = :tagId");
        query.setParameter("tagId", tagId);
        return query.list();
    }

    @Override
    public void create(Beer beer) {
        Session session = setupSession();
        session.save(beer);
    }

    @Override
    public Beer update(int id, Beer beer) {
        // Create a session(connection) in DB.
        Session session = setupSession();

        // Create an SQL query for the DB.
        Query query = session.createQuery("update Beer set name = :name, abv = :abv, picture = :picture," +
                " description = :description, style = :style, country = : country," + "brewery = :brewery where id = :id");

        query.setParameter("id", id);
        query.setParameter("name", beer.getName());
        query.setParameter("brewery", beer.getBrewery());
        query.setParameter("country", beer.getCountry());
        query.setParameter("abv", beer.getAbv());
        query.setParameter("description", beer.getDescription());
        query.setParameter("style", beer.getStyle());
        query.setParameter("picture", beer.getPicture());

        // Send the query to DB.
        query.executeUpdate();

        return beer;
    }

    @Override
    public void updateAverageRating(int beerid, double averageRating) {
        Session session = setupSession();

        Query query = session.createQuery("update Beer set averageRating = :averageRating where id = :beerid");
        query.setParameter("averageRating", averageRating);
        query.setParameter("beerid", beerid);
        query.executeUpdate();
    }

    @Override
    public void addTagToBeer(int tagid, int beerid) {
        Session session = setupSession();

        Query query = session.createSQLQuery("insert into beertags (tagid, beerid)" +
                "values (:tagid, :beerid)");
        query.setParameter("tagid", tagid);
        query.setParameter("beerid", beerid);
        query.executeUpdate();
    }

    @Override
    public void delete(int id) {
        Session session = setupSession();
        Query query = session.createQuery("delete Beer where id = :id");
        query.setParameter( "id", id);
        query.executeUpdate();
    }

    @Override
    public void deleteWishesBeers(int userid, int beerid) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createSQLQuery("delete from user_beers_want_to_drink_rel where userid = :userid and beerid = :beerid");
        query.setParameter("userid", userid);
        query.setParameter("beerid", beerid);
        query.executeUpdate();
    }

    private Session setupSession() {
        Session session;
        try {
            session = sessionFactory.getCurrentSession();
        } catch (Exception e) {
            session = sessionFactory.openSession();
        }
        return session;
    }

}
