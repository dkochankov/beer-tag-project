package com.telerikacademy.beertagproject.repositories;

import com.telerikacademy.beertagproject.models.Beer;
import com.telerikacademy.beertagproject.models.Rating;
import com.telerikacademy.beertagproject.models.User;
import com.telerikacademy.beertagproject.repositories.interfaces.UserRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public class UserRepositoryImpl implements UserRepository {
    private SessionFactory sessionFactory;

    @Autowired
    public UserRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }


    @Override
    public List<User> listUsers() {
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        Query<User> query = session.createQuery("from User", User.class);
        return query.list();
    }

    @Override
    public User getUserById(int id) {
        Session session = sessionFactory.openSession();
        return session.get(User.class, id);
    }

    @Override
    public User getUserByName(String name) {
        Session session = sessionFactory.getCurrentSession();
        Query<User> query = session.createQuery("from User where username = :name", User.class);
        query.setParameter("name", name);
        return query.list().stream().findFirst().orElse(null);
    }

    @Override
    public void createUser(User user, String authority) {
        Session session = sessionFactory.getCurrentSession();
        session.save(user);
        Query query = session.createSQLQuery("insert into authorities (userid, authority) values (:userid, :authority)");
        query.setParameter("userid", user.getId());
        query.setParameter("authority", authority);
        query.executeUpdate();
    }

    @Override
    public void updateUser(int id, User user, String authority) {
        Session session = sessionFactory.getCurrentSession();

        Query query = session.createSQLQuery("delete from authorities where userid = :id");
        query.setParameter("id", id);
        query.executeUpdate();

        query = session.createSQLQuery("insert into authorities (userid, authority) values (:userid, :authority)");
        query.setParameter("userid", user.getId());
        query.setParameter("authority", authority);
        query.executeUpdate();

        query = session.createSQLQuery("update users set " +
                        "password = :password, picture = :picture, " +
                "mail = :mail, countryid = :country where userid = :id");
        //Query query = session.createQuery("update User set " +
        //        "password = :password, picture = :picture, mail = :mail, countryid = :country where userid = :id");
        query.setParameter("id", id);
        //query.setParameter("userName", user.getUsername());
        query.setParameter("password", user.getPassword());
        query.setParameter("picture", user.getPicture());
        query.setParameter("mail", user.getMail());
        query.setParameter("country", user.getCountry().getId());
        query.executeUpdate();
        //return user;

    }

    @Override
    public void deleteUser(int id) {
        Session session = sessionFactory.getCurrentSession();

        Query query = session.createQuery("delete from Rating  where userid = :id");
        query.setParameter("id", id);
        query.executeUpdate();

        query = session.createSQLQuery("delete from authorities where userid = :id");
        query.setParameter("id", id);
        query.executeUpdate();

        query = session.createQuery("delete from User where userid = :id");
        query.setParameter("id", id);
        query.executeUpdate();

    }

    @Override
    public void addToDrankList(int userid, int beerid) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createSQLQuery("insert into user_beers_drank_rel (userid, beerid)" +
                "values (:userid, :beerid)");
        query.setParameter("userid", userid);
        query.setParameter("beerid", beerid);
        query.executeUpdate();
    }

    @Override
    public void addToWantToDrinkList(int userid, int beerid) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createSQLQuery("insert into user_beers_want_to_drink_rel (userid, beerid)" +
                "values (:userid, :beerid)");
        query.setParameter("userid", userid);
        query.setParameter("beerid", beerid);
        query.executeUpdate();
    }

    @Override
    public List<Beer> getDrankList(int userid) {
        Session session = sessionFactory.getCurrentSession();
        Query<Beer> query = session.createNativeQuery("SELECT b.*\n" +
                "FROM beers b inner join user_beers_drank_rel ub on b.beerid = ub.beerid inner join users u on u.userid = ub.userid where u.userid = :userid", Beer.class);
        query.setParameter("userid", userid);
        return query.list();
    }

    @Override
    public List<Beer> getWantToDrinkList(int userid) {
        Session session = sessionFactory.getCurrentSession();
        Query<Beer> query = session.createNativeQuery("SELECT b.*\n" +
                "FROM beers b inner join user_beers_want_to_drink_rel ub on b.beerid = ub.beerid inner join users u on u.userid = ub.userid where u.userid = :userid", Beer.class);
        query.setParameter("userid", userid);
        return query.list();
    }

    @Override
    public void addRating(int rating, int userid, int beerid) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createSQLQuery("insert into ratings (rating, userid, beerid)" +
                "values (:rating, :userid, :beerid)");
        query.setParameter("rating", rating);
        query.setParameter("userid", userid);
        query.setParameter("beerid", beerid);
        query.executeUpdate();
    }

    @Override
    public void updateRating(int rating, int userid, int beerid) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createSQLQuery("update ratings set rating = :rating where userid = :userid and beerid = :beerid");
        query.setParameter("rating", rating);
        query.setParameter("userid", userid);
        query.setParameter("beerid", beerid);
        query.executeUpdate();
    }

    @Override
    public List<Rating> getRating(int userid, int beerid) {
        Session session = sessionFactory.getCurrentSession();
        Query<Rating> query = session.createNativeQuery("SELECT r.*\n" +
                "FROM ratings r where r.userid = :userid and r.beerid = :beerid", Rating.class);
        query.setParameter("userid", userid);
        query.setParameter("beerid", beerid);
        return query.list();
    }

}
