package com.telerikacademy.beertagproject.repositories;

import com.telerikacademy.beertagproject.models.Country;
import com.telerikacademy.beertagproject.repositories.interfaces.CountryRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public class CountryRepositoryImpl implements CountryRepository {
    private SessionFactory sessionFactory;

    @Autowired
    public CountryRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Country getCountryById(int id) {
        Session session = sessionFactory.getCurrentSession();
        return session.get(Country.class, id);
    }

    @Override
    public Country getCountryByName(String name) {
        Session session = sessionFactory.getCurrentSession();
        Query<Country> query = session.createQuery("from Country where country = :name");
        query.setParameter("name", name);
        return query.list().stream().findFirst().orElse(null);
    }

    @Override
    public List<Country> getAll() {
        Session session = sessionFactory.getCurrentSession();
        Query<Country> query = session.createQuery("from Country", Country.class);
        return query.list();
    }

    @Override
    public void create(Country country) {
        Session session = sessionFactory.getCurrentSession();
        session.save(country);
    }

    @Override
    public Country update(int id, Country country) {
        // Create a session(connection) in DB.
        Session session = sessionFactory.getCurrentSession();

        // Create an SQL query for the DB.
        Query query = session.createQuery("update Country set country = :name where id = :id");

        query.setParameter("id", id);
        query.setParameter("name", country.getCountry());

        // Send the query to DB.
        query.executeUpdate();

        return country;
    }

    @Override
    public void delete(int id) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("delete Country where id = :id");
        query.setParameter( "id", id);
        query.executeUpdate();
    }

}
