package com.telerikacademy.beertagproject.repositories;

import com.telerikacademy.beertagproject.models.Brewery;
import com.telerikacademy.beertagproject.repositories.interfaces.BreweryRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public class BreweryRepositoryImpl implements BreweryRepository {
    private SessionFactory sessionFactory;

    @Autowired
    public BreweryRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Brewery getBreweryById(int id) {
        Session session = sessionFactory.getCurrentSession();
        return session.get(Brewery.class, id);
    }

    @Override
    public List<Brewery> getAll() {
        Session session = sessionFactory.getCurrentSession();
        Query<Brewery> query = session.createQuery("from Brewery", Brewery.class);
        return query.list();
    }

    @Override
    public void create(Brewery brewery) {
        Session session = sessionFactory.getCurrentSession();
        session.save(brewery);
    }

    @Override
    public Brewery update(int id, Brewery brewery) {
        // Create a session(connection) in DB.
        Session session = sessionFactory.getCurrentSession();

        // Create an SQL query for the DB.
        Query query = session.createQuery("update Brewery set brewery = :name where id = :id");

        query.setParameter("id", id);
        query.setParameter("name", brewery.getBrewery());

        // Send the query to DB.
        query.executeUpdate();

        return brewery;
    }

    @Override
    public void delete(int id) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("delete Brewery where id = :id");
        query.setParameter( "id", id);
        query.executeUpdate();
    }
}
