package com.telerikacademy.beertagproject.repositories;

import com.telerikacademy.beertagproject.models.Rating;
import com.telerikacademy.beertagproject.models.Tag;
import com.telerikacademy.beertagproject.repositories.interfaces.RatingRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public class RatingRepositoryImpl implements RatingRepository {
    private SessionFactory sessionFactory;

    @Autowired
    public RatingRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }


    @Override
    public List<Rating> getAll() {
        Session session = sessionFactory.getCurrentSession();
        Query<Rating> query = session.createQuery("from Rating", Rating.class);
        return query.list();
    }

    @Override
    public Rating getRatingById(int id) {
        Session session = sessionFactory.getCurrentSession();
        return session.get(Rating.class, id);
    }

    @Override
    public Rating getRatingByUserIdandBeerId(int userid, int beerid) {
        Session session = sessionFactory.getCurrentSession();
        Query<Rating> query = session.createNativeQuery("select from ratings where userid = :userid and beerid=:beerid", Rating.class);
        query.setParameter("userid", userid);
        query.setParameter("beerid", beerid);
        return query.list().stream().findFirst().orElse(null);
    }

    @Override
    public List<Rating> getBeerRatings(int beerid) {
        Session session = sessionFactory.getCurrentSession();
        Query<Rating> query = session.createQuery("from Rating where beerid = :beerid", Rating.class);
        query.setParameter("beerid", beerid);
        return query.list();
    }

    @Override
    public void create(Rating rating) {
        Session session = sessionFactory.getCurrentSession();
        session.save(rating);
    }

    @Override
    public Rating update(int id, Rating rating) {
        // Create a session(connection) in DB.
        Session session = sessionFactory.getCurrentSession();

        // Create an SQL query for the DB.
        Query query = session.createQuery("update Rating set rating = :rating where id = :id");

        query.setParameter("id", id);
        query.setParameter("rating", rating.getRating());

        // Send the query to DB.
        query.executeUpdate();

        return rating;
    }

    @Override
    public void delete(int id) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("delete Rating where id = :id");
        query.setParameter( "id", id);
        query.executeUpdate();
    }

}
