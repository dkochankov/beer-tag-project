package com.telerikacademy.beertagproject.repositories;

import com.telerikacademy.beertagproject.models.Tag;
import com.telerikacademy.beertagproject.repositories.interfaces.TagRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public class TagRepositoryImpl implements TagRepository {
    private SessionFactory sessionFactory;

    @Autowired
    public TagRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Tag> getAll() {
        Session session = sessionFactory.getCurrentSession();
        Query<Tag> query = session.createQuery("from Tag", Tag.class);
        return query.list();
    }

    @Override
    public Tag getTagById(int id) {
        Session session = sessionFactory.getCurrentSession();
        return session.get(Tag.class, id);
    }

    @Override
    public Tag getTagByName(String name) {
        Session session = sessionFactory.getCurrentSession();
        Query<Tag> query = session.createQuery("from Tag where tag = :name");
        query.setParameter("name", name);
        return query.list().stream().findFirst().orElse(null);
    }

    @Override
    public List<Tag> getTagsByBeerId(int id) {
        Session session = sessionFactory.getCurrentSession();
        Query<Tag> query = session.createNativeQuery("select t.* from tags t inner join beertags bt on t.tagid = bt.tagid inner join beers b on b.beerid = bt.beerid where b.beerid = :id", Tag.class);
        query.setParameter("id", id);
        return query.list();
    }

    @Override
    public void create(Tag tag) {
        Session session = sessionFactory.getCurrentSession();
        session.save(tag);
    }

    @Override
    public Tag update(int id, Tag tag) {
        // Create a session(connection) in DB.
        Session session = sessionFactory.getCurrentSession();

        // Create an SQL query for the DB.
        Query query = session.createQuery("update Tag set tag = :tag where id = :id");

        query.setParameter("id", id);
        query.setParameter("tag", tag.getTag());

        // Send the query to DB.
        query.executeUpdate();

        return tag;
    }

    @Override
    public void delete(int id) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("delete Tag where id = :id");
        query.setParameter( "id", id);
        query.executeUpdate();
    }


}
