package com.telerikacademy.beertagproject.repositories;

import com.telerikacademy.beertagproject.models.Style;
import com.telerikacademy.beertagproject.repositories.interfaces.StyleRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public class StyleRepositoryImpl implements StyleRepository {
    private SessionFactory sessionFactory;

    @Autowired
    public StyleRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Style getStyleById(int id) {
        Session session = sessionFactory.getCurrentSession();
        return session.get(Style.class, id);
    }

    @Override
    public Style getStyleByName(String name) {
        Session session = sessionFactory.getCurrentSession();
        Query<Style> query = session.createQuery("from Style where style = :name");
        query.setParameter("name", name);
        return query.list().stream().findFirst().orElse(null);
    }

    @Override
    public List<Style> getAll() {
        Session session = sessionFactory.getCurrentSession();
        Query<Style> query = session.createQuery("from Style", Style.class);
        return query.list();
    }

    @Override
    public void create(Style style) {
        Session session = sessionFactory.getCurrentSession();
        session.save(style);
    }

    @Override
    public Style update(int id, Style style) {
        // Create a session(connection) in DB.
        Session session = sessionFactory.getCurrentSession();

        // Create an SQL query for the DB.
        Query query = session.createQuery("update Style set style = :style where id = :id");

        query.setParameter("id", id);
        query.setParameter("style", style.getStyle());

        // Send the query to DB.
        query.executeUpdate();

        return style;
    }

    @Override
    public void delete(int id) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("delete Style where id = :id");
        query.setParameter( "id", id);
        query.executeUpdate();
    }

}