package com.telerikacademy.beertagproject.repositories.interfaces;

import com.telerikacademy.beertagproject.models.Style;

import java.util.List;

public interface StyleRepository {
    Style getStyleById(int id);
    Style getStyleByName(String name);
    List<Style> getAll();
    void create(Style style);
    Style update(int id, Style style);
    void delete(int id);
}
