package com.telerikacademy.beertagproject.repositories.interfaces;

import com.telerikacademy.beertagproject.models.Beer;

import java.util.List;

public interface BeerRepository {
    Beer getBeerById(int id);
    List<Beer> getAll();
    List<Beer> getAllSortByABV();
    List<Beer> getAllSortByAlphabetical();
    void create(Beer beer);
    Beer update(int id, Beer beer);

    void updateAverageRating(int beerid, double averageRating);
    void addTagToBeer(int tagid, int beerid);

    void delete(int id);
    List<Beer> getAllFilteredByCountry(int countryId);
    List<Beer> getAllFilteredByStyle(int styleId);
    List<Beer> getAllFilteredByTag(int tagId);
    List<Beer> getAllSortByAverageRating();
    void deleteWishesBeers(int userid, int beerid);
}
