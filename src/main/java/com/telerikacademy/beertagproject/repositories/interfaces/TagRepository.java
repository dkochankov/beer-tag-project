package com.telerikacademy.beertagproject.repositories.interfaces;

import com.telerikacademy.beertagproject.models.Tag;

import java.util.List;

public interface TagRepository {
    List<Tag> getAll();
    Tag getTagById(int id);
    Tag getTagByName(String name);
    List<Tag> getTagsByBeerId(int id);
    void create(Tag tag);
    Tag update(int id, Tag tag);
    void delete(int id);
}
