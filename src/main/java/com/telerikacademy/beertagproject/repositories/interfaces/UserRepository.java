package com.telerikacademy.beertagproject.repositories.interfaces;

import com.telerikacademy.beertagproject.models.Beer;
import com.telerikacademy.beertagproject.models.Rating;
import com.telerikacademy.beertagproject.models.User;

import java.util.List;

public interface UserRepository {
    List<User> listUsers();
    User getUserById(int id);
    User getUserByName(String name);
    void createUser(User user, String authority);
    void updateUser(int id, User user, String authority);
    void deleteUser(int id);

    void addToDrankList(int beerid, int userid);
    void addToWantToDrinkList(int beerid, int userid);
    void addRating(int rating, int userid, int beerid);
    void updateRating(int rating, int userid, int beerid);
    List<Rating> getRating(int userid, int beerid);
    List<Beer> getDrankList(int userid);
    List<Beer> getWantToDrinkList(int userid);
}
