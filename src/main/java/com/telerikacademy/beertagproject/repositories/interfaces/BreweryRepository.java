package com.telerikacademy.beertagproject.repositories.interfaces;

import com.telerikacademy.beertagproject.models.Brewery;

import java.util.List;

public interface BreweryRepository {
    Brewery getBreweryById(int id);
    List<Brewery> getAll();
    void create(Brewery brewery);
    Brewery update(int id, Brewery brewery);
    void delete(int id);
}
