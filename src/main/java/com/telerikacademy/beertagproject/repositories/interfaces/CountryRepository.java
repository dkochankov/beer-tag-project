package com.telerikacademy.beertagproject.repositories.interfaces;

import com.telerikacademy.beertagproject.models.Country;

import java.util.List;

public interface CountryRepository {
    Country getCountryById(int id);
    Country getCountryByName(String name);
    List<Country> getAll();
    void create(Country country);
    Country update(int id, Country country);
    void delete(int id);
}
