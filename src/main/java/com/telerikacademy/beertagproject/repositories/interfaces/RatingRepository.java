package com.telerikacademy.beertagproject.repositories.interfaces;

import com.telerikacademy.beertagproject.models.Rating;

import java.util.List;

public interface RatingRepository {
    List<Rating> getAll();
    Rating getRatingById(int id);
    Rating getRatingByUserIdandBeerId(int userid, int beerid);
    List<Rating> getBeerRatings(int beerid);
    void create(Rating rating);
    Rating update(int id, Rating rating);
    void delete(int id);
}
