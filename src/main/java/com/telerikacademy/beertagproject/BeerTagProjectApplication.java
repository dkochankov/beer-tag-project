package com.telerikacademy.beertagproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BeerTagProjectApplication {

    public static void main(String[] args) {
        SpringApplication.run(BeerTagProjectApplication.class, args);
    }

}
