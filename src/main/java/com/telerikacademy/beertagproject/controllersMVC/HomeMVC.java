package com.telerikacademy.beertagproject.controllersMVC;

import com.telerikacademy.beertagproject.models.User;
import com.telerikacademy.beertagproject.services.CountryServiceImpl;
import com.telerikacademy.beertagproject.services.UserServiceImpl;
import com.telerikacademy.beertagproject.services.interfaces.CountryService;
import com.telerikacademy.beertagproject.services.interfaces.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;

@Controller
public class HomeMVC {
    private UserService userService;
    private CountryService countryService;

    @Autowired
    public HomeMVC(UserService userService, CountryService countryService) {
        this.userService = userService;
        this.countryService = countryService;
    }

    @GetMapping
    public String showHomePage() {
        return "redirect:/beers";
    }

    @GetMapping("/register")
    public String showRegisterPage(Model model) {
        model.addAttribute("user", new User());
        model.addAttribute("countries", countryService.getAll());
        return "register";
    }

    @PostMapping("/register")
    public String registerUser(@Valid @ModelAttribute User user,
                               BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("error", "Username/password can't be empty");
            return "register";
        }
        String password = "{noop}" + user.getPassword();
        user.setPassword(password);

        userService.createUser(user, "ROLE_USER");
        return "redirect:/beers";
    }

}
