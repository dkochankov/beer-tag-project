package com.telerikacademy.beertagproject.controllersMVC;

import com.telerikacademy.beertagproject.models.*;
import com.telerikacademy.beertagproject.services.*;
import com.telerikacademy.beertagproject.services.interfaces.CountryService;
import com.telerikacademy.beertagproject.services.interfaces.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.security.Principal;

@Controller
@RequestMapping
public class UserMVC {
    private UserService userService;
    private CountryService countryService;

    @Autowired
    public UserMVC(UserService userService, CountryService countryService) {
        this.userService = userService;
        this.countryService = countryService;
    }

    @GetMapping("/users")
    public String showUsers(Model model) {
        model.addAttribute("users", userService.listUsers());
        return "users";
    }

    @GetMapping("/getUserById/{id}")
    public String getUserById(Model model, @PathVariable int id) {
        model.addAttribute("user", userService.getUserById(id));
        model.addAttribute("beersdrank", userService.getDrankList(id));
        model.addAttribute("beerswanttodrink", userService.getWantToDrinkList(id));
        return "userdetails";
    }

    @GetMapping("/createUser")
    public String showNewEmployeeForm(Model model) {
        model.addAttribute("user", new User());
        model.addAttribute("countries", countryService.getAll());
        return "createuser";
    }

    @PostMapping("/createUser")
    public String createUser(@ModelAttribute User user,
                             @ModelAttribute("country") String country,
                             @RequestParam String authority,
                             RedirectAttributes redirectAttributes) {
        String password = "{noop}" + user.getPassword();
        user.setPassword(password);
        try {
            userService.createUser(user, authority);
        } catch (IllegalArgumentException e) {
            redirectAttributes.addFlashAttribute("createUserExceptionMessage", e.getMessage());
            return "redirect:/users";
        }
        redirectAttributes.addFlashAttribute("createdUserMessage", "user created!");
        return "redirect:/users";
    }

    @GetMapping("/editUser/{id}")
    public String showEditUserForm(Model model, @PathVariable int id) {
        model.addAttribute("user", userService.getUserById(id));
        model.addAttribute("countries", countryService.getAll());
        return "edituser";
    }

    @PutMapping("/editUser/{id}")
    public String editUser(@PathVariable int id,
                           @ModelAttribute User user,
                           @ModelAttribute("country") String country,
                           @RequestParam String authority,
                           RedirectAttributes redirectAttributes) {
        try {
            userService.updateUser(id, user, authority);
        } catch (IllegalArgumentException e) {
            redirectAttributes.addFlashAttribute("editUserExceptionMessage", e.getMessage());
            return "redirect:/users";
        }
        redirectAttributes.addFlashAttribute("updatedUserMessage", "user updated!");
        return "redirect:/users";
    }

    @GetMapping("/deleteUser/{id}")
    public String deleteUser(@PathVariable int id, RedirectAttributes redirectAttributes) {
        userService.deleteUser(id);
        redirectAttributes.addFlashAttribute("deletedUserMessage", "user deleted!");
        return "redirect:/users";
    }

    @GetMapping("/addToDrankList/{beerid}")
    public String addToDrankList(@PathVariable int beerid, Principal principal, RedirectAttributes redirectAttributes) {
        String name = principal.getName();
        int userid = userService.getUserByName(name).getId();
        try {
            userService.addToDrankList(userid, beerid);
        } catch (IllegalArgumentException e) {
            redirectAttributes.addFlashAttribute("addToDrankListExceptionMessage", e.getMessage());
            return "redirect:/beers";
        }
        redirectAttributes.addFlashAttribute("addToDrankListMessage", "Beer added to drank list!");
        return "redirect:/beers";
    }

    @GetMapping("/addToWantToDrinkList/{beerid}")
    public String addToWantToDrinkList(@PathVariable int beerid, Principal principal, RedirectAttributes redirectAttributes) {
        String name = principal.getName();
        int userid = userService.getUserByName(name).getId();
        try {
            userService.addToWantToDrinkList(userid, beerid);
        } catch (IllegalArgumentException e) {
            redirectAttributes.addFlashAttribute("addToWantToDrinkListExceptionMessage", e.getMessage());
            return "redirect:/beers";
        }
        redirectAttributes.addFlashAttribute("addToWantToDrinkListMessage", "Beer added to want to drink list!");
        return "redirect:/beers";
    }

    @PostMapping("/addRating/{beerid}")
    public String addRating(@PathVariable int beerid, Principal principal,
                             @RequestParam int rating, RedirectAttributes redirectAttributes) {
        String name = principal.getName();
        int userid = userService.getUserByName(name).getId();
        userService.addRating(rating, userid, beerid);
        redirectAttributes.addFlashAttribute("addRatingMessage", "Rating added!");

        return "redirect:/beers";
    }

    @GetMapping("/addTag/{beerid}")
    public String addTag(@PathVariable int beerid, @RequestParam String tag, RedirectAttributes redirectAttributes) {
        try {
            userService.addTag(tag, beerid);
        } catch (IllegalArgumentException e) {
            redirectAttributes.addFlashAttribute("addTagExceptionMessage", e.getMessage());
            return "redirect:/beers";
        }
        redirectAttributes.addFlashAttribute("addTagMessage", "Tag added!");
        return "redirect:/beers";
    }

}

