package com.telerikacademy.beertagproject.controllersMVC;

import com.telerikacademy.beertagproject.models.Beer;
import com.telerikacademy.beertagproject.services.interfaces.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.security.Principal;
import java.util.List;

@Controller
@RequestMapping
public class BeerMVC {
    private BeerService beerService;
    private UserService userService;
    private CountryService countryService;
    private StyleService styleService;
    private BreweryService breweryService;
    private TagService tagService;

    @Autowired
    public BeerMVC(BeerService beerService, UserService userService, CountryService countryService,
                   StyleService styleService, BreweryService breweryService, TagService tagService) {
        this.beerService = beerService;
        this.userService = userService;
        this.countryService = countryService;
        this.styleService = styleService;
        this.breweryService = breweryService;
        this.tagService = tagService;
    }

    @GetMapping("/beers")
    public String showBeers(Model model,
                            Principal principal,
                            @RequestParam(required = false) String sortBy,
                            @RequestParam(required = false) String country,
                            @RequestParam(required = false) String style,
                            @RequestParam(required = false) String tag
                            ) {
        List<Beer> beers;
        if (country != null) {
            beers = beerService.getAllFilteredByCountry(country);
        } else if (style != null) {
            beers = beerService.getAllFilteredByStyle(style);
        } else if (tag != null) {
            beers = beerService.getAllFilteredByTag(tag);
        } else if (sortBy != null) {
            beers = beerService.getAll(sortBy);
        } else {
            beers = beerService.getAll();
        }

        if (principal != null) {
            String username = principal.getName();
            int userid = userService.getUserByName(username).getId();
            model.addAttribute("beersdrank", userService.getDrankList(userid));
            model.addAttribute("beerswanttodrink", userService.getWantToDrinkList(userid));
            model.addAttribute("user", userService.getUserById(userid));
        }

        model.addAttribute("beers", beers);

        return "beers";
    }

    @GetMapping("/beers/getBeerById/{id}")
    public String getBeerById(Model model, @PathVariable int id) {
        model.addAttribute("beer", beerService.getBeerById(id));
        model.addAttribute("tags", tagService.getTagsByBeerId(id));
        return "beerdetails";
    }

    @GetMapping("/createBeer")
    public String showNewBeerForm(Model model) {
        model.addAttribute("beer", new Beer());
        model.addAttribute("countries", countryService.getAll());
        model.addAttribute("breweries", breweryService.getAll());
        model.addAttribute("styles", styleService.getAll());
        return "createbeer";
    }

    @PostMapping("/createBeer")
    public String createBeer(@ModelAttribute Beer beer,
                             @ModelAttribute("country") String country,
                             @ModelAttribute("style") String style,
                             @ModelAttribute("brewery") String brewery,
                             RedirectAttributes redirectAttributes) {
        try {
            beerService.create(beer);
        }
        catch (IllegalArgumentException e) {
            redirectAttributes.addFlashAttribute("createBeerExceptionMessage", e.getMessage() + ". Problem creating a beer.");
            return "redirect:/beers";
        }
        redirectAttributes.addFlashAttribute("createdBeerMessage", "Beer created!");
        return "redirect:/beers";
    }

    @GetMapping("/editBeer/{id}")
    public String showEditBeerForm(Model model, @PathVariable int id) {
        model.addAttribute("beer", beerService.getBeerById(id));
        model.addAttribute("countries", countryService.getAll());
        model.addAttribute("breweries", breweryService.getAll());
        model.addAttribute("styles", styleService.getAll());
        return "editbeer";
    }

    @PutMapping("/editBeer/{id}")
    public String editBeer(@PathVariable int id,
                           @ModelAttribute Beer beer,
                           @ModelAttribute("country") String country,
                           @ModelAttribute("style") String style,
                           @ModelAttribute("brewery") String brewery,
                           RedirectAttributes redirectAttributes) {
        try {
            beerService.update(id, beer);
        } catch (IllegalArgumentException e) {
            redirectAttributes.addFlashAttribute("editBeerExceptionMessage", e.getMessage());
            return "redirect:/beerdetails";
        }
        redirectAttributes.addFlashAttribute("updatedBeerMessage", "beer updated!");
        return "redirect:/beerdetails";
    }

    @GetMapping("/deleteBeer/{id}")
    public String deleteBeer(@PathVariable int id, RedirectAttributes redirectAttributes) {
        beerService.delete(id);
        redirectAttributes.addFlashAttribute("deletedBeerMessage", "beer deleted!");
        return "redirect:/beers";
    }

    @GetMapping("/deleteWishesBeers/{beerid}")
    public String deleteWishesBeers(@PathVariable int beerid, Principal principal, RedirectAttributes redirectAttributes) {
        String name = principal.getName();
        int userid = userService.getUserByName(name).getId();
        try {
            beerService.deleteWishesBeers(userid, beerid);
        } catch (IllegalArgumentException e) {
            redirectAttributes.addFlashAttribute("deleteWishesBeersExceptionMessage", e.getMessage());
            return "redirect:/beers";
        }
        redirectAttributes.addFlashAttribute("deletedBeerMessage", "beer deleted!");
        return "redirect:/beers";
    }

}
