-- MySQL dump 10.17  Distrib 10.3.15-MariaDB, for osx10.12 (x86_64)
--
-- Host: 127.0.0.1    Database: beerproject
-- ------------------------------------------------------
-- Server version	10.3.15-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

CREATE DATABASE IF NOT EXISTS `beerproject` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;
USE `beerproject`;

--
-- Table structure for table `authorities`
--

DROP TABLE IF EXISTS `authorities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `authorities` (
  `userid` int(10) unsigned NOT NULL,
  `authority` varchar(100) NOT NULL,
  UNIQUE KEY `authorities_userid_authority_uindex` (`userid`,`authority`),
  CONSTRAINT `authorities_users_userid_fk` FOREIGN KEY (`userid`) REFERENCES `users` (`userid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `authorities`
--

LOCK TABLES `authorities` WRITE;
/*!40000 ALTER TABLE `authorities` DISABLE KEYS */;
INSERT INTO `authorities` VALUES (9,'ROLE_USER'),(17,'ROLE_ADMIN'),(30,'ROLE_USER'),(31,'ROLE_USER'),(32,'ROLE_USER'),(33,'ROLE_USER'),(34,'ROLE_USER'),(35,'ROLE_USER'),(36,'ROLE_ADMIN');
/*!40000 ALTER TABLE `authorities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `beers`
--

DROP TABLE IF EXISTS `beers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `beers` (
  `beerid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `beername` varchar(100) NOT NULL,
  `abv` double unsigned NOT NULL,
  `picture` varchar(100) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `averagerating` double DEFAULT NULL,
  `countryid` int(10) unsigned NOT NULL,
  `breweryid` int(10) unsigned NOT NULL,
  `styleid` int(10) unsigned NOT NULL,
  PRIMARY KEY (`beerid`),
  KEY `beers_fk` (`styleid`),
  KEY `beers_fk_1` (`countryid`),
  KEY `beers_fk_2` (`breweryid`),
  CONSTRAINT `beers_fk` FOREIGN KEY (`styleid`) REFERENCES `styles` (`styleid`),
  CONSTRAINT `beers_fk_1` FOREIGN KEY (`countryid`) REFERENCES `countries` (`countryid`),
  CONSTRAINT `beers_fk_2` FOREIGN KEY (`breweryid`) REFERENCES `breweries` (`breweryid`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `beers`
--

LOCK TABLES `beers` WRITE;
/*!40000 ALTER TABLE `beers` DISABLE KEYS */;
INSERT INTO `beers` VALUES (1,'Kamenitza Light',4.5,'https://res.cloudinary.com/ratebeer/image/upload/e_trim:1/d_beer_img_default.png,f_auto/beer_11944','Bulgarian original kamenitza',4,1,1,1),(2,'Zagorka Special',5,'https://res.cloudinary.com/ratebeer/image/upload/e_trim:1/d_beer_img_default.png,f_auto/beer_11946','Zagorka beer',2.5,1,2,1),(3,'Ariana Light',5,'https://res.cloudinary.com/ratebeer/image/upload/e_trim:1/d_beer_img_default.png,f_auto/beer_11948','Ariana Light beer',3.5,1,2,1),(4,'Ariana Dark',5.5,'https://res.cloudinary.com/ratebeer/image/upload/e_trim:1/d_beer_img_default.png,f_auto/beer_13937','Ariana Dark beer',2,1,3,5),(5,'Ariana Radler lemon',1.8,'https://res.cloudinary.com/ratebeer/image/upload/e_trim:1/d_beer_img_default.png,f_auto/beer_181649','Ariana Radler beer',4,1,2,6),(6,'Zagorka Reserve',6,'https://res.cloudinary.com/ratebeer/image/upload/e_trim:1/d_beer_img_default.png,f_auto/beer_161906','Zagorka Reserve beer',0,1,2,5),(7,'Zagorka Fusion',2,'https://res.cloudinary.com/ratebeer/image/upload/e_trim:1/d_beer_img_default.png,f_auto/beer_180189','Zagorka Fusion beer',3.5,1,2,6),(8,'Atella Artois',4.8,'https://res.cloudinary.com/ratebeer/image/upload/e_trim:1/d_beer_img_default.png,f_auto/beer_1478','Atella Artois beer',3,3,4,8),(9,'Beck\'s 1873',6,'https://res.cloudinary.com/ratebeer/image/upload/e_trim:1/d_beer_img_default.png,f_auto/beer_322237','Beck\'s beer',3,2,5,8),(10,'Beck\'s',4.9,'https://res.cloudinary.com/ratebeer/image/upload/e_trim:1/d_beer_img_default.png,f_auto/beer_703','Beck\'s beer',0,2,5,11),(11,'Corona Extra',4.5,'https://res.cloudinary.com/ratebeer/image/upload/e_trim:1/d_beer_img_default.png,f_auto/beer_742','Corona Extra beer',0,4,6,1),(12,'Carlsberg',5,'https://res.cloudinary.com/ratebeer/image/upload/e_trim:1/d_beer_img_default.png,f_auto/beer_709','Carlsberg beer',4,5,7,1),(13,'Tuborg',4,'https://res.cloudinary.com/ratebeer/image/upload/e_trim:1/d_beer_img_default.png,f_auto/beer_3201','Tuborg beer',0,5,7,1),(14,'Black Star',4.5,'https://res.cloudinary.com/ratebeer/image/upload/e_trim:1/d_beer_img_default.png,f_auto/beer_505','Black Star beer',0,6,8,8),(15,'Over The Hill',3.5,'https://res.cloudinary.com/ratebeer/image/upload/e_trim:1/d_beer_img_default.png,f_auto/beer_276889','Over The Hill beer',0,7,9,5),(16,'Black Isle Goldfinch',3.5,'https://res.cloudinary.com/ratebeer/image/upload/e_trim:1/d_beer_img_default.png,f_auto/beer_404336','Black Isle Goldfinch beer',0,8,10,3),(17,'Heineken',5,'https://res.cloudinary.com/ratebeer/image/upload/e_trim:1/d_beer_img_default.png,f_auto/beer_37','Heineken beer',1.5,9,11,1),(18,'Staropramen',4,'https://res.cloudinary.com/ratebeer/image/upload/e_trim:1/d_beer_img_default.png,f_auto/beer_3784','Staropramen beer',0,10,12,12),(19,'Bernard',4.5,'https://res.cloudinary.com/ratebeer/image/upload/e_trim:1/d_beer_img_default.png,f_auto/beer_9579','Bernard beer',0,10,13,1),(20,'Bernard Amber',5,'https://res.cloudinary.com/ratebeer/image/upload/e_trim:1/d_beer_img_default.png,f_auto/beer_79302','Bernard Amber beer',0,10,13,5),(21,'Starobrno',5,'https://res.cloudinary.com/ratebeer/image/upload/e_trim:1/d_beer_img_default.png,f_auto/beer_5217','Starobrno beer',0,10,14,1),(22,'Leffe Brune',6.5,'https://res.cloudinary.com/ratebeer/image/upload/e_trim:1/d_beer_img_default.png,f_auto/beer_2516','Leffe Brown beer',0,3,4,13),(24,'Rocheforst Trappis',11.3,'https://res.cloudinary.com/ratebeer/image/upload/e_trim:1/d_beer_img_default.png,f_auto/beer_2360','The top product from the Rochefort Trappist brewery.',0,3,8,13);
/*!40000 ALTER TABLE `beers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `beertags`
--

DROP TABLE IF EXISTS `beertags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `beertags` (
  `tagid` int(11) unsigned NOT NULL,
  `beerid` int(11) unsigned NOT NULL,
  PRIMARY KEY (`tagid`,`beerid`),
  KEY `beertags_beers_beerid_fk` (`beerid`),
  CONSTRAINT `beertags_beers_beerid_fk` FOREIGN KEY (`beerid`) REFERENCES `beers` (`beerid`),
  CONSTRAINT `beertags_tags_tagid_fk` FOREIGN KEY (`tagid`) REFERENCES `tags` (`tagid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `beertags`
--

LOCK TABLES `beertags` WRITE;
/*!40000 ALTER TABLE `beertags` DISABLE KEYS */;
INSERT INTO `beertags` VALUES (1,1),(1,9),(2,2),(2,10),(3,3),(3,11),(3,19),(4,4),(4,12),(5,5),(5,13),(5,20),(6,6),(6,14),(7,7),(7,15),(7,21),(8,8),(8,16),(9,17),(9,18),(9,22);
/*!40000 ALTER TABLE `beertags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `breweries`
--

DROP TABLE IF EXISTS `breweries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `breweries` (
  `breweryid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `brewery` varchar(100) NOT NULL,
  PRIMARY KEY (`breweryid`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `breweries`
--

LOCK TABLES `breweries` WRITE;
/*!40000 ALTER TABLE `breweries` DISABLE KEYS */;
INSERT INTO `breweries` VALUES (1,'Plovdiv'),(2,'Stara Zagora'),(3,'Sofia'),(4,'Leuven'),(5,'Bermen'),(6,'Cervecería Modelo'),(7,'Copenhagen'),(8,'The Great Northern'),(9,'Longhope'),(10,'Black Isle'),(11,'Amsterdam'),(12,'Prague'),(13,'Humpolec'),(14,'Brno'),(15,'Brasserie Rochefort');
/*!40000 ALTER TABLE `breweries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `countries`
--

DROP TABLE IF EXISTS `countries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `countries` (
  `countryid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `country` varchar(100) NOT NULL,
  PRIMARY KEY (`countryid`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `countries`
--

LOCK TABLES `countries` WRITE;
/*!40000 ALTER TABLE `countries` DISABLE KEYS */;
INSERT INTO `countries` VALUES (1,'Bulgaria'),(2,'Germany'),(3,'Belgium'),(4,'Mexico'),(5,'Denmark'),(6,'USA'),(7,'United Kingdom'),(8,'Scotland'),(9,'Netherlands'),(10,'Czech Republic');
/*!40000 ALTER TABLE `countries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ratings`
--

DROP TABLE IF EXISTS `ratings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ratings` (
  `ratingid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `rating` int(10) unsigned NOT NULL,
  `userid` int(10) unsigned NOT NULL,
  `beerid` int(10) unsigned NOT NULL,
  PRIMARY KEY (`ratingid`),
  UNIQUE KEY `ratings_userid_beerid_uindex` (`userid`,`beerid`),
  KEY `ratings_beers_beerid_fk` (`beerid`),
  CONSTRAINT `ratings_beers_beerid_fk` FOREIGN KEY (`beerid`) REFERENCES `beers` (`beerid`),
  CONSTRAINT `ratings_users_userid_fk` FOREIGN KEY (`userid`) REFERENCES `users` (`userid`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ratings`
--

LOCK TABLES `ratings` WRITE;
/*!40000 ALTER TABLE `ratings` DISABLE KEYS */;
INSERT INTO `ratings` VALUES (6,5,30,5),(11,4,30,7),(16,4,34,1),(17,1,34,2),(18,2,34,3),(19,2,34,17),(21,3,34,8),(22,2,34,4);
/*!40000 ALTER TABLE `ratings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `styles`
--

DROP TABLE IF EXISTS `styles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `styles` (
  `styleid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `style` varchar(100) NOT NULL,
  PRIMARY KEY (`styleid`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `styles`
--

LOCK TABLES `styles` WRITE;
/*!40000 ALTER TABLE `styles` DISABLE KEYS */;
INSERT INTO `styles` VALUES (1,'Pale Lager'),(2,'Dark IPA'),(3,'Pale IPA'),(4,'Strong Lager'),(5,'Dark Lager'),(6,'Radler Style'),(7,'Ale'),(8,'Pilsner'),(9,'Bock'),(10,'Wheat beer'),(11,'Lager'),(12,'Pale Draught'),(13,'Dark Ale');
/*!40000 ALTER TABLE `styles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tags`
--

DROP TABLE IF EXISTS `tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tags` (
  `tagid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tag` varchar(255) NOT NULL,
  PRIMARY KEY (`tagid`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tags`
--

LOCK TABLES `tags` WRITE;
/*!40000 ALTER TABLE `tags` DISABLE KEYS */;
INSERT INTO `tags` VALUES (1,'#awesome'),(2,'#beautful'),(3,'#sour'),(4,'#great'),(5,'#fresh'),(6,'#summer pleasure'),(7,'#cooling'),(8,'#addictive'),(9,'#relaxing');
/*!40000 ALTER TABLE `tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_beers_drank_rel`
--

DROP TABLE IF EXISTS `user_beers_drank_rel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_beers_drank_rel` (
  `userid` int(10) unsigned NOT NULL,
  `beerid` int(10) unsigned NOT NULL,
  PRIMARY KEY (`userid`,`beerid`),
  KEY `users_beer_drank_rel_fk_1` (`beerid`),
  CONSTRAINT `users_beer_drank_rel_fk` FOREIGN KEY (`userid`) REFERENCES `users` (`userid`),
  CONSTRAINT `users_beer_drank_rel_fk_1` FOREIGN KEY (`beerid`) REFERENCES `beers` (`beerid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_beers_drank_rel`
--

LOCK TABLES `user_beers_drank_rel` WRITE;
/*!40000 ALTER TABLE `user_beers_drank_rel` DISABLE KEYS */;
INSERT INTO `user_beers_drank_rel` VALUES (17,5),(32,1),(33,1),(33,2),(34,1),(34,3),(34,4);
/*!40000 ALTER TABLE `user_beers_drank_rel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_beers_want_to_drink_rel`
--

DROP TABLE IF EXISTS `user_beers_want_to_drink_rel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_beers_want_to_drink_rel` (
  `userid` int(10) unsigned NOT NULL,
  `beerid` int(10) unsigned NOT NULL,
  PRIMARY KEY (`userid`,`beerid`),
  KEY `FK_users_beers_want_to_drink_rel_beers` (`beerid`),
  CONSTRAINT `FK_users_beers_want_to_drink_rel_beers` FOREIGN KEY (`beerid`) REFERENCES `beers` (`beerid`),
  CONSTRAINT `FK_users_beers_want_to_drink_rel_users` FOREIGN KEY (`userid`) REFERENCES `users` (`userid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_beers_want_to_drink_rel`
--

LOCK TABLES `user_beers_want_to_drink_rel` WRITE;
/*!40000 ALTER TABLE `user_beers_want_to_drink_rel` DISABLE KEYS */;
INSERT INTO `user_beers_want_to_drink_rel` VALUES (17,1),(17,2),(32,1),(32,9),(33,1),(33,2),(34,1),(34,2),(34,5),(34,7);
/*!40000 ALTER TABLE `user_beers_want_to_drink_rel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `userid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `picture` varchar(100) DEFAULT NULL,
  `mail` varchar(100) NOT NULL,
  `countryid` int(10) unsigned NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  PRIMARY KEY (`userid`),
  KEY `users_fk` (`countryid`),
  CONSTRAINT `users_fk` FOREIGN KEY (`countryid`) REFERENCES `countries` (`countryid`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (9,'Gosho','{noop}123456','picture','gosho@abv.bg',1,1),(17,'Vasko','{noop}123456','picture','vasko@abv.bg',2,1),(30,'Mimi','{noop}123456','snimk','email@dev.bg',1,1),(31,'Toni','{noop}123456','picture','toni@abv.bg',1,1),(32,'Ned','{noop}123456','picture','ned@abv.bg',1,1),(33,'Gery','{noop}123456','picture','gery@abv.bg',2,1),(34,'Krisi','{noop}123456','picture','krisi@email.com',1,1),(35,'Mitko','{noop}123456','picture','mitko@abv.bg',1,1),(36,'Kosta','{noop}123456','snimka','email@dev.bg',1,1);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-07-08 14:51:01
