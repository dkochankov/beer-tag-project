package com.telerikacademy.beertagproject.services;
import com.telerikacademy.beertagproject.models.Tag;
import com.telerikacademy.beertagproject.repositories.interfaces.TagRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class TagServiceImplTest {
    @Mock
    TagRepository mockTagRepository;

    @InjectMocks
    TagServiceImpl tagService;

    Tag mockTag = new Tag("#fresh");

    @Test
    public void get_All_Should_Return_A_List_Of_Beers() {
        //Arrange
        Mockito.when(mockTagRepository.getAll())
                .thenReturn(Arrays.asList(mockTag));
        //Act
        List<Tag> tags = tagService.getAll();
        //Assert
        Assert.assertEquals(1, tags.size());
    }

    @Test
    public void get_Tag_By_Id_Should_Return_A_Beer_When_Id_Matches() {
        Mockito.when(mockTagRepository.getTagById(1))
                .thenReturn(new Tag("#awesome"));

        Tag tag = tagService.getTagById(1);

        Assert.assertEquals("#awesome", tag.getTag());
    }

    @Test(expected = IllegalArgumentException.class)
    public void get_Tag_By_Id_Should_Return_An_Exception_When_Id_Doesnt_Match() {
        Tag tag = tagService.getTagById(2);
    }

    @Test
    public void create_Should_Call_Repository_Create() {
        Tag tag = new Tag();
        tagService.create(tag);
        Mockito.verify(mockTagRepository, Mockito.times(1)).create(tag);
    }

    @Test(expected = IllegalArgumentException.class)
    public void get_Tag_create_Should_Return_An_Exception_When_Tag_Exists() {
        //Arrange
        Mockito.when(mockTagRepository.getAll())
                .thenReturn(Arrays.asList(mockTag));

        //Act
        tagService.create(mockTag);

        //Assert - Da sme sigurni, che ne syzdavame tag.
        Mockito.verify(mockTagRepository, Mockito.times(0)).create(mockTag);
    }

    @Test
    public void update_Should_Call_Repository_Update() {
        Tag tag = new Tag();
        tagService.update(1, tag);
        Mockito.verify(mockTagRepository, Mockito.times(1)).update(1, tag);
    }

    @Test
    public void delete_Should_Call_Repository_Delete() {
        tagService.delete(1);
        Mockito.verify(mockTagRepository, Mockito.times(1)).delete(1);
    }
}
