package com.telerikacademy.beertagproject.services;

import com.telerikacademy.beertagproject.models.*;
import com.telerikacademy.beertagproject.repositories.interfaces.BeerRepository;
import com.telerikacademy.beertagproject.repositories.interfaces.CountryRepository;
import com.telerikacademy.beertagproject.repositories.interfaces.StyleRepository;
import com.telerikacademy.beertagproject.repositories.interfaces.TagRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import java.util.Arrays;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class BeerServiceImplTests {
    @Mock
    BeerRepository mockBeerRepository;

    @Mock
    CountryRepository mockCountryRepository;

    @Mock
    StyleRepository mockStyleRepository;

    @Mock
    TagRepository mockTagRepository;

    @InjectMocks
    BeerServiceImpl beerService;

    Country mockCountry = new Country("Bulgaria");
    Style mockStyle = new Style("Dark IPA");
    Brewery mockBrewery = new Brewery("Kamenitza Brewery");
    Tag mockTag = new Tag("#awesome");

    @Test
    public void get_All_Should_Return_A_List_Of_Beers() {

        //Arrange
        Mockito.when(mockBeerRepository.getAll())
                .thenReturn(Arrays.asList(
                        new Beer("Kamenitza", 4.5, "picture.jpg", "Amazing beer",
                                4.75, mockStyle, mockCountry, mockBrewery)));
        //Act
        List<Beer> beers = beerService.getAll();

        //Assert
        Assert.assertEquals(1, beers.size());
    }

    @Test
    public void get_Beer_By_Id_Should_Return_A_Beer_When_Id_Matches() {
        Mockito.when(mockBeerRepository.getBeerById(1))
                .thenReturn(new Beer("Kamenitza", 4.5, "picture.jpg", "Amazing beer",
                        4.75, mockStyle, mockCountry, mockBrewery));

        Beer beer = beerService.getBeerById(1);

        Assert.assertEquals("Kamenitza", beer.getName());
    }

    @Test(expected = IllegalArgumentException.class)
    public void get_Beer_By_Id_Should_Return_An_Exception_When_Id_Doesnt_Match() {
        Beer beer = beerService.getBeerById(1);
    }

    @Test
    public void get_All_Sort_By_ABV_Should_Return_A_List_Of_Beers() {
        Mockito.when(mockBeerRepository.getAllSortByABV())
                .thenReturn(Arrays.asList(
                        new Beer("Kamenitza", 4, "picture.jpg", "Amazing beer",
                                5, mockStyle, mockCountry, mockBrewery),
                        new Beer("Ariana", 4.5, "picture.jpg", "Great beer",
                                4.5, mockStyle, mockCountry, mockBrewery),
                        new Beer("Beck's", 5, "picture.jpg", "Awesome",
                                4, mockStyle, mockCountry, mockBrewery)));

        List<Beer> beers = beerService.getAll("abv");

        Assert.assertEquals(4, beers.get(0).getAbv(), 0);
        Assert.assertEquals(4.5, beers.get(1).getAbv(), 0);
        Assert.assertEquals(5, beers.get(2).getAbv(), 0);
    }

    @Test
    public void get_All_Sort_By_Alphabetical_Should_Return_A_List_Of_Beers() {
        Mockito.when(mockBeerRepository.getAllSortByAlphabetical())
                .thenReturn(Arrays.asList(
                        new Beer("Ariana", 4, "picture.jpg", "Amazing beer",
                                5, mockStyle, mockCountry, mockBrewery),
                        new Beer("Beck's", 4.5, "picture.jpg", "Great beer",
                                4.5, mockStyle, mockCountry, mockBrewery),
                        new Beer("Kamenitza", 5, "picture.jpg", "Awesome",
                                4, mockStyle, mockCountry, mockBrewery)));

        List<Beer> beers = beerService.getAll("alphabetical");

        Assert.assertEquals("Ariana", beers.get(0).getName());
        Assert.assertEquals("Beck's", beers.get(1).getName());
        Assert.assertEquals("Kamenitza", beers.get(2).getName());
    }

    @Test
    public void get_All_Sort_By_Rating_Should_Return_A_List_Of_Beers() {
        Mockito.when(mockBeerRepository.getAllSortByAverageRating())
                .thenReturn(Arrays.asList(
                        new Beer("Kamenitza", 4.5, "picture.jpg", "Amazing beer",
                                5, mockStyle, mockCountry, mockBrewery),
                        new Beer("Ariana", 4, "picture.jpg", "Great beer",
                                4.5, mockStyle, mockCountry, mockBrewery),
                        new Beer("Beck's", 5, "picture.jpg", "Awesome",
                                4, mockStyle, mockCountry, mockBrewery)));

        List<Beer> beers = beerService.getAll("rating");

        Assert.assertEquals(5.0, beers.get(0).getAverageRating(), 0);
        Assert.assertEquals(4.5, beers.get(1).getAverageRating(), 0);
        Assert.assertEquals(4.0, beers.get(2).getAverageRating(), 0);
    }

    @Test
    public void get_All_Filtered_By_Country_Should_Return_A_List_Of_Beers() {
        Mockito.when(mockCountryRepository.getCountryByName("Bulgaria"))
                .thenReturn(mockCountry);
        Mockito.when(mockBeerRepository.getAllFilteredByCountry(mockCountry.getId()))
                .thenReturn(Arrays.asList(
                        new Beer("Kamenitza", 4.5, "picture.jpg", "Amazing beer",
                                5, mockStyle, mockCountry, mockBrewery)));

        List<Beer> beers = beerService.getAllFilteredByCountry("Bulgaria");

        Assert.assertEquals("Kamenitza", beers.get(0).getName());
    }

    @Test(expected = IllegalArgumentException.class)
    public void get_All_Filtered_By_Country_Should_Return_Exception_When_Id_Doesnt_Match() {
        List<Beer> beers = beerService.getAllFilteredByCountry("Bulgaria");
    }

    @Test
    public void get_All_Filtered_By_Style_Should_Return_A_List_Of_Beers() {
        Mockito.when(mockStyleRepository.getStyleByName("Dark IPA"))
                .thenReturn(mockStyle);
        Mockito.when(mockBeerRepository.getAllFilteredByStyle(mockStyle.getId()))
                .thenReturn(Arrays.asList(
                        new Beer("Kamenitza", 4.5, "picture.jpg", "Amazing beer",
                                5, mockStyle, mockCountry, mockBrewery)));

        List<Beer> beers = beerService.getAllFilteredByStyle("Dark IPA");

        Assert.assertEquals("Kamenitza", beers.get(0).getName());
    }

    @Test(expected = IllegalArgumentException.class)
    public void get_All_Filtered_By_Style_Should_Return_Exception_When_Id_Doesnt_Match() {
        List<Beer> beers = beerService.getAllFilteredByStyle("Dark IPA");
    }

    @Test
    public void get_All_Filtered_By_Tag_Should_Return_A_List_Of_Beers() {
        Mockito.when(mockTagRepository.getTagByName("#awesome"))
                .thenReturn(mockTag);
        Mockito.when(mockBeerRepository.getAllFilteredByTag(mockTag.getId()))
                .thenReturn(Arrays.asList(
                        new Beer("Kamenitza", 4.5, "picture.jpg", "Amazing beer",
                                5, mockStyle, mockCountry, mockBrewery)));

        List<Beer> beers = beerService.getAllFilteredByTag("#awesome");

        Assert.assertEquals("Kamenitza", beers.get(0).getName());
    }

    @Test(expected = IllegalArgumentException.class)
    public void get_All_Filtered_By_Tag_Should_Return_Exception_When_Id_Doesnt_Match() {
        List<Beer> beers = beerService.getAllFilteredByTag("#awesome");
    }

    @Test
    public void create_Should_Call_Repository_Create() {
        Beer beer = new Beer("Kamenitza", 4.5, "picture.jpg", "Amazing beer",
                5, mockStyle, mockCountry, mockBrewery);
        beerService.create(beer);
        Mockito.verify(mockBeerRepository, Mockito.times(1)).create(beer);
    }

    @Test(expected = IllegalArgumentException.class)
    public void get_Beer_create_Should_Return_An_Exception_When_Beer_Exists() {
        //Arrange
        Mockito.when(mockBeerRepository.getAll())
                .thenReturn(Arrays.asList(new Beer("Kamenitza", 4.5, "picture.jpg", "Amazing beer",
                        5, mockStyle, mockCountry, mockBrewery)));

        //Act
        beerService.create(new Beer("Kamenitza", 4.5, "picture.jpg", "Amazing beer",
                5, mockStyle, mockCountry, mockBrewery));

        //Assert - Da sme sigurni, che ne syzdavame beer.
        Mockito.verify(mockBeerRepository, Mockito.times(0))
                .create(new Beer("Kamenitza", 4.5, "picture.jpg", "Amazing beer",
                5, mockStyle, mockCountry, mockBrewery));
    }

    @Test
    public void update_Should_Call_Repository_Update() {
        Beer beer = new Beer("Kamenitza", 4.5, "picture.jpg", "Amazing beer",
                5, mockStyle, mockCountry, mockBrewery);
        beerService.update(1, beer);
        Mockito.verify(mockBeerRepository, Mockito.times(1)).update(1, beer);
    }

    @Test
    public void delete_Should_Call_Repository_Delete() {
        beerService.delete(1);
        Mockito.verify(mockBeerRepository, Mockito.times(1)).delete(1);
    }

}
