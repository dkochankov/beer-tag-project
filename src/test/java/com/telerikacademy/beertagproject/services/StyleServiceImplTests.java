package com.telerikacademy.beertagproject.services;

import com.telerikacademy.beertagproject.models.Beer;
import com.telerikacademy.beertagproject.models.Style;
import com.telerikacademy.beertagproject.repositories.interfaces.StyleRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class StyleServiceImplTests {
    @Mock
    StyleRepository mockStyleRepository;

    @InjectMocks
    StyleServiceImpl styleService;

    Style mockStyle = new Style("Pilsner");

    @Test
    public void get_All_Should_Return_A_List_Of_Beers() {
        //Arrange
        Mockito.when(mockStyleRepository.getAll())
                .thenReturn(Arrays.asList(mockStyle));
        //Act
        List<Style> styles = styleService.getAll();
        //Assert
        Assert.assertEquals(1, styles.size());
    }

    @Test
    public void create_Should_Call_Repository_Create() {
        Style style = new Style();
        styleService.create(style);
        Mockito.verify(mockStyleRepository, Mockito.times(1)).create(style);
    }

    @Test(expected = IllegalArgumentException.class)
    public void get_Style_create_Should_Return_An_Exception_When_Style_Exists() {
        //Arrange
        Mockito.when(mockStyleRepository.getAll())
                .thenReturn(Arrays.asList(mockStyle));

        //Act
        styleService.create(mockStyle);

        //Assert - Da sme sigurni, che ne syzdavame style.
        Mockito.verify(mockStyleRepository, Mockito.times(0)).create(mockStyle);
    }

    @Test
    public void update_Should_Call_Repository_Update() {
        Style style = new Style();
        styleService.update(1, style);
        Mockito.verify(mockStyleRepository, Mockito.times(1)).update(1, style);
    }

    @Test
    public void delete_Should_Call_Repository_Delete() {
        styleService.delete(1);
        Mockito.verify(mockStyleRepository, Mockito.times(1)).delete(1);
    }

}
