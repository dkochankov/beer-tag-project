package com.telerikacademy.beertagproject.services;
import com.telerikacademy.beertagproject.models.Brewery;
import com.telerikacademy.beertagproject.repositories.interfaces.BreweryRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class BreweryServiceImplTests {
    @Mock
    BreweryRepository mockBreweryRepository;

    @InjectMocks
    BreweryServiceImpl breweryService;

    Brewery mockBrewery = new Brewery("Plovdiv");

    @Test
    public void get_All_Should_Return_A_List_Of_Beers() {
        //Arrange
        Mockito.when(mockBreweryRepository.getAll())
                .thenReturn(Arrays.asList(mockBrewery));
        //Act
        List<Brewery> breweries = breweryService.getAll();
        //Assert
        Assert.assertEquals(1, breweries.size());
    }

    @Test
    public void create_Should_Call_Repository_Create() {
        Brewery brewery = new Brewery();
        breweryService.create(brewery);
        Mockito.verify(mockBreweryRepository, Mockito.times(1)).create(brewery);
    }

    @Test(expected = IllegalArgumentException.class)
    public void get_Tag_create_Should_Return_An_Exception_When_Brewery_Exists() {
        //Arrange
        Mockito.when(mockBreweryRepository.getAll())
                .thenReturn(Arrays.asList(mockBrewery));

        //Act
        breweryService.create(mockBrewery);

        //Assert - Da sme sigurni, che ne syzdavame brewery.
        Mockito.verify(mockBreweryRepository, Mockito.times(0)).create(mockBrewery);
    }

    @Test
    public void update_Should_Call_Repository_Update() {
        Brewery brewery = new Brewery();
        breweryService.update(1, brewery);
        Mockito.verify(mockBreweryRepository, Mockito.times(1)).update(1, brewery);
    }

    @Test
    public void delete_Should_Call_Repository_Delete() {
        breweryService.delete(1);
        Mockito.verify(mockBreweryRepository, Mockito.times(1)).delete(1);
    }
}
