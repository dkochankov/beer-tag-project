package com.telerikacademy.beertagproject.services;

import com.telerikacademy.beertagproject.models.*;
import com.telerikacademy.beertagproject.repositories.BeerRepositoryImpl;
import com.telerikacademy.beertagproject.repositories.RatingRepositoryImpl;
import com.telerikacademy.beertagproject.repositories.UserRepositoryImpl;
import com.telerikacademy.beertagproject.services.interfaces.UserService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceImplTests {

    @Mock
    private UserRepositoryImpl mockUserRepository;

    @Mock
    private RatingRepositoryImpl mockRatingRepository;

    @Mock
    private BeerRepositoryImpl mockBeerRepository;

    @InjectMocks
    UserServiceImpl userService;

    Country mockCountry = new Country("Bulgaria");
    String authority = "ADMIN";
    Style mockStyle = new Style("Dark IPA");
    Brewery mockBrewery = new Brewery("Kamenitza Brewery");

    User mockUser = new User("Nasko", "123456", "picture.jpg", "nasko@abv.bg",
            mockCountry);
    Beer mockBeer = new Beer("Kamenitza", 4.5, "picture.jpg", "Amazing beer",
            4.75, mockStyle, mockCountry, mockBrewery);


    @Test
    public void listUsers_Should_Return_A_List_Of_Users() {

        //Arrange
        Mockito.when(mockUserRepository.listUsers())
                .thenReturn(Arrays.asList(
                        new User("Vasko", "123456", "picture.jpg", "vasko@abv.bg",
                                mockCountry)));
        //Act
        List<User> users = userService.listUsers();

        //Assert
        Assert.assertEquals(1, users.size());
    }

    @Test
    public void getUserById_Should_Return_A_User_When_Id_Matches() {
        Mockito.when(mockUserRepository.getUserById(1))
                .thenReturn(new User("Nasko", "123456", "picture.jpg", "nasko@abv.bg",
                        mockCountry));

        User user = userService.getUserById(1);

        Assert.assertEquals("Nasko", user.getUsername());
    }

    @Test(expected = IllegalArgumentException.class)
    public void getUserById_Should_Return_An_Exception_When_Id_Doesnt_Match() {
        User user = userService.getUserById(2);
    }

    @Test
    public void getUserByName_Should_Return_A_User_When_Name_Matches() {
        Mockito.when(mockUserRepository.getUserByName("Nasko"))
                .thenReturn(new User("Nasko", "123456", "picture.jpg", "nasko@abv.bg",
                        mockCountry));

        User user = userService.getUserByName("Nasko");

        Assert.assertEquals("Nasko", user.getUsername());
    }

    @Test(expected = IllegalArgumentException.class)
    public void getUserByName_Should_Return_An_Exception_When_Name_Doesnt_Match() {
        User user = userService.getUserByName("ZZZZ");
    }

    @Test
    public void create_Should_Call_Repository_Create() {
        User user = new User("Pesho", "123456", "picture.jpg", "pesho@abv.bg",
                mockCountry);
        userService.createUser(user, authority);
        Mockito.verify(mockUserRepository, Mockito.times(1)).createUser(user, authority);
    }

    @Test
    public void update_Should_Call_Repository_Update() {
        User user = new User("Ivan", "12346", "picture.jpg", "ivan@abv.bg",
                mockCountry);
        userService.updateUser(1, user, authority);
        Mockito.verify(mockUserRepository, Mockito.times(1)).updateUser(1, user, authority);
    }

    @Test
    public void delete_Should_Call_Repository_Delete() {
        userService.deleteUser(1);
        Mockito.verify(mockUserRepository, Mockito.times(1)).deleteUser(1);
    }

    @Test
    public void addToWantToDrinkList_Should_Call_The_User_repository(){
        Mockito.when(mockUserRepository.getWantToDrinkList(1))
                .thenReturn(Arrays.asList(mockBeer));

        userService.addToWantToDrinkList(1, 1);

        Mockito.verify(mockUserRepository, Mockito.times(1)).addToWantToDrinkList(1, 1);
    }

    @Test
    public void addToDrankList_Should_Call_The_User_repository(){
        Mockito.when(mockUserRepository.getDrankList(1))
                .thenReturn(Arrays.asList(mockBeer));

        userService.addToDrankList(1, 1);

        Mockito.verify(mockUserRepository, Mockito.times(1)).addToDrankList(1, 1);
    }

}
