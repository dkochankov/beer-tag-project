package com.telerikacademy.beertagproject.services;

import com.telerikacademy.beertagproject.models.*;
import com.telerikacademy.beertagproject.repositories.interfaces.RatingRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class RatingServiceImplTests {
    @Mock
    RatingRepository mockRatingRepository;

    @InjectMocks
    RatingServiceImpl ratingService;


    Country mockCountry = new Country("Bulgaria");
    Style mockStyle = new Style("Dark IPA");
    Brewery mockBrewery = new Brewery("Kamenitza Brewery");

    User mockUser = new User("Nasko", "123456", "picture.jpg", "nasko@abv.bg",
            mockCountry);
    Beer mockBeer = new Beer("Kamenitza", 4.5, "picture.jpg", "Amazing beer",
            4.75, mockStyle, mockCountry, mockBrewery);
    Rating mockRating = new Rating(4, mockUser, mockBeer);

    @Test
    public void get_All_Should_Return_A_List_Of_Beers() {
        //Arrange
        Mockito.when(mockRatingRepository.getAll())
                .thenReturn(Arrays.asList(mockRating));
        //Act
        List<Rating> rating = ratingService.getAll();
        //Assert
        Assert.assertEquals(1, rating.size());
    }

    @Test
    public void get_Rating_By_Id_Should_Return_A_Beer_When_Id_Matches() {
        Mockito.when(mockRatingRepository.getRatingById(1))
                .thenReturn(mockRating);

        Rating rating = ratingService.getRatingById(1);

        Assert.assertEquals(4, rating.getRating());
    }

    @Test(expected = IllegalArgumentException.class)
    public void get_Rating_By_Id_Should_Return_An_Exception_When_Id_Doesnt_Match() {
        Rating rating = ratingService.getRatingById(2);
    }

    @Test
    public void create_Should_Call_Repository_Create() {
        Rating rating = new Rating();
        ratingService.create(rating);
        Mockito.verify(mockRatingRepository, Mockito.times(1)).create(rating);
    }

    @Test(expected = IllegalArgumentException.class)
    public void get_Rating_create_Should_Return_An_Exception_When_Rating_Exists() {
        //Arrange
        Mockito.when(mockRatingRepository.getAll())
                .thenReturn(Arrays.asList(mockRating));

        //Act
        ratingService.create(mockRating);

        //Assert - Da sme sigurni, che ne syzdavame rating.
        Mockito.verify(mockRatingRepository, Mockito.times(0)).create(mockRating);
    }

    @Test
    public void update_Should_Call_Repository_Update() {
        Rating rating = new Rating();
        ratingService.update(1, rating);
        Mockito.verify(mockRatingRepository, Mockito.times(1)).update(1, rating);
    }

    @Test
    public void delete_Should_Call_Repository_Delete() {
        ratingService.delete(1);
        Mockito.verify(mockRatingRepository, Mockito.times(1)).delete(1);
    }
}
