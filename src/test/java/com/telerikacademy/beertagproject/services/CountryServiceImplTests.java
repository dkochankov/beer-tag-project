package com.telerikacademy.beertagproject.services;

import com.telerikacademy.beertagproject.models.Country;
import com.telerikacademy.beertagproject.repositories.interfaces.CountryRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class CountryServiceImplTests {
    @Mock
    CountryRepository mockCountryRepository;

    @InjectMocks
    CountryServiceImpl countryService;

    Country mockCountry = new Country("Bulgaria");

    @Test
    public void get_All_Should_Return_A_List_Of_Beers() {
        //Arrange
        Mockito.when(mockCountryRepository.getAll())
                .thenReturn(Arrays.asList(mockCountry));
        //Act
        List<Country> countries = countryService.getAll();
        //Assert
        Assert.assertEquals(1, countries.size());
    }

    @Test
    public void create_Should_Call_Repository_Create() {
        Country country = new Country();
        countryService.create(country);
        Mockito.verify(mockCountryRepository, Mockito.times(1)).create(country);
    }

    @Test
    public void update_Should_Call_Repository_Update() {
        Country country = new Country();
        countryService.update(1, country);
        Mockito.verify(mockCountryRepository, Mockito.times(1)).update(1, country);
    }

    @Test
    public void delete_Should_Call_Repository_Delete() {
        countryService.delete(1);
        Mockito.verify(mockCountryRepository, Mockito.times(1)).delete(1);
    }
}
